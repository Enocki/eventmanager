ALTER TABLE `teammergesuggestion`
    DROP FOREIGN KEY `FK_teammergesuggestion_team2`,
    DROP FOREIGN KEY `FK_teammergesuggestion_team1`;
ALTER TABLE `teammergesuggestion`
    ADD COLUMN `event_eventid` BIGINT UNSIGNED NOT NULL AFTER `teammergesuggestionid`,
    CHANGE COLUMN `first_team_teamid` `teammergesuggestionid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
    CHANGE COLUMN `second_team_teamid` `team_teamid` BIGINT UNSIGNED NOT NULL ,
    CHANGE COLUMN `merged` `teammerginggroup` BIGINT UNSIGNED NOT NULL ,
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (`teammergesuggestionid`),
    DROP INDEX `FK_teammergesuggestion_team2` ,
    ADD INDEX `FK_teammergesuggestion_team` (`team_teamid` ASC),
    ADD INDEX `FK_teammergesuggestion_event_idx` (`event_eventid` ASC),
    ADD UNIQUE INDEX `UQ_teammergesuggestion_team` (`team_teamid` ASC);
ALTER TABLE `teammergesuggestion`
    ADD CONSTRAINT `FK_teammergesuggestion_team`
        FOREIGN KEY (`team_teamid`)
            REFERENCES `team` (`teamid`),
    ADD CONSTRAINT `FK_teammergesuggestion_event`
        FOREIGN KEY (`event_eventid`)
            REFERENCES `event` (`eventid`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION;
