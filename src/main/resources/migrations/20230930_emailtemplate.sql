CREATE TABLE `emailtemplate` (
    `emailtemplateid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `code` VARCHAR(45) NULL,
    `content` TEXT NOT NULL,
    `subject` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`emailtemplateid`),
    UNIQUE INDEX `UQ_emailtemplate_code` (`code` ASC)
);

INSERT INTO `emailtemplate` (code, content, subject)
VALUES
    ("magic-link", "Lien de connexion à EventManager: <a href=\"{magicLink}\">{magicLink}</a>", "Lien de connexion"),
    ("password-reset", "Lien de réinitialisation de votre mot de passe: <a href=\"{passwordResetLink}\">{passwordResetLink}</a>", "Reinitialisation de votre mot de passe");

