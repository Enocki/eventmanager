ALTER TABLE `user`
    ADD COLUMN `passwordresettoken` VARCHAR(32) NULL DEFAULT NULL AFTER `password`,
    ADD COLUMN `passwordresettokencreationdate` DATETIME(6) NULL DEFAULT NULL AFTER `passwordresettoken`,
    ADD UNIQUE INDEX `UQ_user_passwordresettoken` (`passwordresettoken` ASC);
