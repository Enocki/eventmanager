ALTER TABLE `user`
    ADD COLUMN `city_cityid` INT UNSIGNED NULL DEFAULT NULL AFTER `userid`,
    ADD COLUMN `diploma_diplomaid` INT UNSIGNED NULL DEFAULT NULL AFTER `city_cityid`,
    ADD INDEX `FK_user_city_idx` (`city_cityid` ASC),
    ADD INDEX `FK_user_diploma_idx` (`diploma_diplomaid` ASC);

ALTER TABLE `user`
    ADD CONSTRAINT `FK_user_city`
        FOREIGN KEY (`city_cityid`)
            REFERENCES `city` (`cityid`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    ADD CONSTRAINT `FK_user_diploma`
        FOREIGN KEY (`diploma_diplomaid`)
            REFERENCES `diploma` (`diplomaid`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION;
