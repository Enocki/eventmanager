ALTER TABLE `eventdocumentrequirement`
    ADD COLUMN `document_documentid` BIGINT UNSIGNED NULL AFTER `eventdocumentrequirementid`,
ADD INDEX `FK_eventdocumentrequirement_document_idx` (`document_documentid` ASC);

ALTER TABLE `eventdocumentrequirement`
    ADD CONSTRAINT `FK_eventdocumentrequirement_document`
        FOREIGN KEY (`document_documentid`)
            REFERENCES `document` (`documentid`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION;
