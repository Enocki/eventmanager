UPDATE emailtemplate SET content = REPLACE(content, '{lastname}', '{nom}');
UPDATE emailtemplate SET content = REPLACE(content, '{firstname}', '{prenom}');
UPDATE emailtemplate SET content = REPLACE(content, '{eventName}', '{nom evenement}');
UPDATE emailtemplate SET content = REPLACE(content, '{teamName}', '{nom equipe}');
UPDATE emailtemplate SET content = REPLACE(content, '{emailConfirmLink}', '{lien de confirmation}');
UPDATE emailtemplate SET content = REPLACE(content, '{magicLink}', '{lien de connexion}');
UPDATE emailtemplate SET content = REPLACE(content, '{passwordResetLink}', '{lien de reinitialisation}');
UPDATE emailtemplate SET content = REPLACE(content, '{teamInvitationLink}', '{lien invitation}');
