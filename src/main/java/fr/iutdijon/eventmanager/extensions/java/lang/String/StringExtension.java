package fr.iutdijon.eventmanager.extensions.java.lang.String;

import manifold.ext.rt.api.Extension;
import manifold.ext.rt.api.This;

@Extension
public class StringExtension {
    public static boolean isNullOrEmpty(@This String input) {
        return input == null || input.isEmpty();
    }

    public static String truncate(@This String input, int maxLength) {
        if (input.length() > maxLength)
            return input.substring(0, maxLength);
        return input;
    }
}
