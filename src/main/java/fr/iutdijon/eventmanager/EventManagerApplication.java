package fr.iutdijon.eventmanager;

import fr.iutdijon.eventmanager.services.log.ILogSingletonService;
import fr.iutdijon.eventmanager.services.migration.IMigrationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.File;

@SpringBootApplication
@EnableAsync
@EnableScheduling
public class EventManagerApplication {

	private final ILogSingletonService logSingletonService;

	@Autowired
	public EventManagerApplication(ILogSingletonService logSingletonService) {
		this.logSingletonService = logSingletonService;
	}

	@Bean("migrationPath")
	public String migrationPath(boolean devMode) {
		return devMode ? "classpath:migrations" : "migrations";
	}

	@Bean("devMode")
	public boolean devMode(Environment environment) {
		return environment.getActiveProfiles().length > 0 && environment.getActiveProfiles()[0].equals("dev");
	}

	@Bean("documentsDirectory")
	public File documentsDirectory() {
		return new File("./documents");
	}

	@Scheduled(initialDelay = 60000, fixedRate = 60000 * 5) // 5 minutes
	public void purgeLogs() {
		logSingletonService.purgeLogs();
	}

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(EventManagerApplication.class, args);

		try {
			context.getBean(IMigrationService.class).doMigration();
		}
		catch (Exception e) {
			e.printStackTrace();
			context.close();
			return;
		}

		if (context.getBean("devMode", boolean.class)) {
			try {
				final Logger logger = LoggerFactory.getLogger(EventManagerApplication.class);

				logger.info("Starting generating API client...");
				final long start = System.currentTimeMillis();
				ProcessBuilder processBuilder = new ProcessBuilder("nswag.cmd", "run", "nswagstudio.nswag");
				processBuilder.directory(new File("."));
				processBuilder.start().waitFor();
				logger.info("API client generated successfully in {} ms.", System.currentTimeMillis() - start);
			}
			catch (Exception e) {
				e.printStackTrace();
				context.close();
			}
		}
	}

}
