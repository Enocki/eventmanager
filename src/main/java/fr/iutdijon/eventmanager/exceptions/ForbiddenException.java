package fr.iutdijon.eventmanager.exceptions;

/**
 * Must be thrown if the user must not execute an action because of his permissions.
 */
public class ForbiddenException extends IllegalStateException {
}
