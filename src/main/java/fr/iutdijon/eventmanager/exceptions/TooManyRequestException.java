package fr.iutdijon.eventmanager.exceptions;

import lombok.Getter;

@Getter
public class TooManyRequestException extends IllegalStateException {
    private final Long retryAfter;
    public TooManyRequestException(Long retryAfter) {
        this.retryAfter = retryAfter;
    }
}
