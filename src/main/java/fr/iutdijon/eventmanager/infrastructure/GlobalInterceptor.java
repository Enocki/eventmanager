package fr.iutdijon.eventmanager.infrastructure;

import fr.iutdijon.eventmanager.EventManagerApplication;
import fr.iutdijon.eventmanager.EventManagerProperties;
import fr.iutdijon.eventmanager.services.user.IUserService;
import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;
import fr.iutdijon.eventmanager.services.user.models.business.UserPermissionBusinessModel;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizeAnonymousPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class GlobalInterceptor implements WebMvcConfigurer, HandlerInterceptor {

    private final String EVENT_MANAGER_PACKAGE = EventManagerApplication.class.getPackage().getName();

    private final ApplicationContext context;
    private final EventManagerProperties properties;

    @Autowired
    public GlobalInterceptor(ApplicationContext context, EventManagerProperties properties) {
        this.context = context;
        this.properties = properties;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(this);
    }

    @Override
    public boolean preHandle(@NotNull HttpServletRequest request, HttpServletResponse response, @NotNull Object handler) {
        // Always write the required headers
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        response.setHeader("Access-Control-Allow-Origin", properties.getUiBaseUrl());
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Expose-Headers", EventManagerHeaders.REMAINING_TRIES_HEADER_NAME + ",Retry-After");

        AuthorizeAnyPolicy authorizeAnyPolicy = null;
        AuthorizeAnonymousPolicy authorizeAnonymousPolicy = null;
        if (handler instanceof HandlerMethod m) {
            String currentPackage = m.getBean().getClass().getPackage().getName();
            if (!currentPackage.startsWith(EVENT_MANAGER_PACKAGE))
                return true;

            authorizeAnyPolicy = m.getBean().getClass().getAnnotation(AuthorizeAnyPolicy.class);
            authorizeAnonymousPolicy = m.getBean().getClass().getAnnotation(AuthorizeAnonymousPolicy.class);

            if (authorizeAnyPolicy != null && authorizeAnonymousPolicy != null)
                throw new IllegalStateException("Cannot have both AuthorizeAnyPolicy and AuthorizeAnonymousPolicy");

            AuthorizeAnyPolicy overrideAuthorizeAnyPolicy = m.getMethod().getAnnotation(AuthorizeAnyPolicy.class);
            AuthorizeAnonymousPolicy overrideAuthorizeAnonymousPolicy = m.getMethod().getAnnotation(AuthorizeAnonymousPolicy.class);

            if (overrideAuthorizeAnyPolicy != null && overrideAuthorizeAnonymousPolicy != null)
                throw new IllegalStateException("Cannot have both AuthorizeAnyPolicy and AuthorizeAnonymousPolicy");

            if (overrideAuthorizeAnyPolicy != null || overrideAuthorizeAnonymousPolicy != null) {
                authorizeAnyPolicy = overrideAuthorizeAnyPolicy;
                authorizeAnonymousPolicy = overrideAuthorizeAnonymousPolicy;
            }
        }
        else
            return true;

        IUserService userService = context.getBean(IUserService.class);
        UserBusinessModel user = userService.getCurrentUser();

        // If AnonymousPolicy
        if (authorizeAnonymousPolicy != null) {
            if (user == null || !authorizeAnonymousPolicy.rejectLoggedUsers())
                return true;

            response.setStatus(403);
            return false;
        }

        // If LoggedPolicy
        if (authorizeAnyPolicy != null) {
            if (user == null) {
                response.setStatus(401);
                return false;
            }

            for (AuthorizationPolicy authorizationPolicy : authorizeAnyPolicy.value()) {
                UserPermissionBusinessModel permissionEquivalent = switch (authorizationPolicy)
                {
                    case ADMINISTRATOR -> UserPermissionBusinessModel.ADMINISTRATOR;
                    case DEVELOPER -> UserPermissionBusinessModel.DEVELOPER;
                    case MODERATOR -> UserPermissionBusinessModel.MODERATOR;
                    default -> null;
                };

                if (permissionEquivalent == null) {
                    if (authorizationPolicy == AuthorizationPolicy.USER)
                        return true;
                    else
                        throw new IllegalStateException("Unknown policy: " + authorizationPolicy);
                }

                if (user.havePermission(permissionEquivalent))
                    return true;
            }

            response.setStatus(403);
            return false;
        }

        response.setStatus(501);
        return false;
    }
}
