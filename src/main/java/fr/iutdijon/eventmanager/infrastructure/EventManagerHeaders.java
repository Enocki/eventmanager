package fr.iutdijon.eventmanager.infrastructure;

public class EventManagerHeaders {
    public static final String REMAINING_TRIES_HEADER_NAME = "X-EventManager-RemainingLoginAttempts";
}
