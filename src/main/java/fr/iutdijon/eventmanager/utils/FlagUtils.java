package fr.iutdijon.eventmanager.utils;

public class FlagUtils {
    public static long addFlag(long number, long binary) {
        return number | binary;
    }

    public static boolean haveFlag(long number, long binary) {
        return (number & binary) > 0;
    }

    public static long removeFlag(long number, long binary) {
        return number & ~binary;
    }
}
