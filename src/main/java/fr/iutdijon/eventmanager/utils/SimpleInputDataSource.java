package fr.iutdijon.eventmanager.utils;

import jakarta.activation.DataSource;
import org.springframework.util.Assert;

import java.io.*;

public class SimpleInputDataSource implements DataSource {
    private final byte[] inputStream;
    private final String contentType;
    private final String name;

    /**
     * Create a simple data source from a row input
     * @param inputStream The content of this data source in byte array
     * @param contentType The data type of the object represented by the byte array
     * @param name The name of the data source
     */
    public SimpleInputDataSource(byte[] inputStream, String contentType, String name) {
        Assert.notNull(inputStream, "InputStream cannot be null");
        Assert.notNull(contentType, "Content-Type cannot be null");
        Assert.notNull(name, "Name cannot be null");

        this.inputStream = inputStream;
        this.contentType = contentType;
        this.name = name;
    }

    /**
     * Create a simple data source from an {@link InputStream}
     * @param inputStream The {@link InputStream} to create the data source from
     * @param contentType The content type of the {@link InputStream}
     * @param name The name of the data source
     * @throws IOException If an error occurs when reading the {@link InputStream}
     */
    public SimpleInputDataSource(InputStream inputStream, String contentType, String name) throws IOException {
        this(inputStream == null ? null : inputStream.readAllBytes(), contentType, name);
    }

    @Override
    public InputStream getInputStream() {
        if (this.inputStream == null)
            throw new UnsupportedOperationException();

        return new ByteArrayInputStream(this.inputStream);
    }

    @Override
    public OutputStream getOutputStream() {
        return ByteArrayOutputStream.nullOutputStream();
    }

    @Override
    public String getContentType() {
        return this.contentType;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
