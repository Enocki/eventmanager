package fr.iutdijon.eventmanager.utils;

public class TokenUtils {
    /**
     * Generate a random {@link String} with a given number of character.<br/>
     * The used alphabet is the union of lower case letters, upper case letters and numbers so 62 characters.
     * @param length The length of the random generated {@link String}
     * @return The generated random {@link String}
     */
    public static String generateRandomString(int length) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int index = (int) Math.floor(Math.random() * 62);
            builder.append("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".charAt(index));
        }
        return builder.toString();
    }
}
