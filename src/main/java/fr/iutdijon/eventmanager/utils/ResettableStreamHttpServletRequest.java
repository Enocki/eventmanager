package fr.iutdijon.eventmanager.utils;

import jakarta.servlet.ServletInputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletRequestWrapper;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class ResettableStreamHttpServletRequest extends HttpServletRequestWrapper {
    private byte[] rawData = {};
    private final HttpServletRequest request;
    private final ResettableServletInputStream servletStream;

    public String requestId;
    public String payloadFilePrefix;
    public String payloadTarget;

    public ResettableStreamHttpServletRequest(HttpServletRequest request) throws IOException {
        super(request);
        this.request = request;
        this.servletStream = new ResettableServletInputStream();
    }

    void resetInputStream() throws IOException {
        initRawData();
        servletStream.inputStream = new ByteArrayInputStream(rawData);
    }

    private void initRawData() throws IOException {
        if ( rawData.length == 0 ) {
            byte[] b = this.request.getInputStream().readAllBytes();
            if ( b != null )
                rawData = b;
        }
        servletStream.inputStream = new ByteArrayInputStream(rawData);
    }
    @Override
    public ServletInputStream getInputStream() throws IOException {
        initRawData();
        return servletStream;
    }
    public BufferedReader getReader() throws IOException {
        initRawData();
        String encoding = getCharacterEncoding();
        if ( encoding != null ) {
            return new BufferedReader(new InputStreamReader(servletStream, encoding));
        } else {
            return new BufferedReader(new InputStreamReader(servletStream));
        }
    }
}
