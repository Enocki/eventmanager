package fr.iutdijon.eventmanager.services.email.models.business;

public enum EmailPlaceholderTypeBusinessModel {
    LINK,
    TEXT;
}
