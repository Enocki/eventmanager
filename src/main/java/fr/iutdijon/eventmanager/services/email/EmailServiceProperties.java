package fr.iutdijon.eventmanager.services.email;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties("event-manager.email")
public class EmailServiceProperties {
    private String emailFrom;
    private String emailDisplayName;
    private String emailReplyTo;
}
