package fr.iutdijon.eventmanager.services.email.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

@Getter
@Setter
public class EmailTemplateDataModel {
    @JDapperColumnName("emailtemplateid")
    private Long id;

    @JDapperColumnName("user_userid")
    private Long ownerId;

    @JDapperColumnName("code")
    private String code;
    @JDapperColumnName("content")
    private String content;
    @JDapperColumnName("label")
    private String label;
    @JDapperColumnName("subject")
    private String subject;
    @JDapperColumnName("shared")
    private Boolean shared;
}
