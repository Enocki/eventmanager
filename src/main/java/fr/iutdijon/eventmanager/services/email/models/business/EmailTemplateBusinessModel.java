package fr.iutdijon.eventmanager.services.email.models.business;

import fr.iutdijon.eventmanager.services.email.models.data.EmailTemplateDataModel;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class EmailTemplateBusinessModel {
    private Long id;

    private Long ownerId;

    private String code;
    private String content;
    private String label;
    private String subject;
    private Boolean shared;

    private List<EmailPlaceholderBusinessModel> placeholders = new ArrayList<>();

    public EmailTemplateDataModel toDataModel() {
        return new EmailTemplateDataModel() {{
            setId(id);
            setOwnerId(ownerId);
            setCode(code);
            setContent(content);
            setLabel(label);
            setSubject(subject);
            setShared(shared);
        }};
    }

    public static EmailTemplateBusinessModel fromDataModel(EmailTemplateDataModel dataModel) {
        return new EmailTemplateBusinessModel() {{
            setId(dataModel.getId());
            setOwnerId(dataModel.getOwnerId());
            setCode(dataModel.getCode());
            setContent(dataModel.getContent());
            setLabel(dataModel.getLabel());
            setSubject(dataModel.getSubject());
            setShared(dataModel.getShared());
        }};
    }
}
