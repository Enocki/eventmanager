package fr.iutdijon.eventmanager.services.email.models.business;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Getter
public enum EmailPlaceholderBusinessModel {
    LASTNAME("Nom", "nom", EmailPlaceholderTypeBusinessModel.TEXT, "Cette valeur sera remplacée par le nom de l'utilisateur recevant l'email."),
    FIRSTNAME("Prénom", "prenom", EmailPlaceholderTypeBusinessModel.TEXT, "Cette valeur sera remplacée par le prénom de l'utilisateur recevant l'email."),
    TEAM_NAME("Nom d'équipe", "nom equipe", EmailPlaceholderTypeBusinessModel.TEXT, "Uniquement disponible si l'email est envoyé à une équipe. Cette valeur sera remplacée par le nom de l'équipe."),
    EVENT_NAME("Nom de l'évènement", "nom evenement", EmailPlaceholderTypeBusinessModel.TEXT, "Uniquement disponible si l'email est envoyé au sein d'un évènement. Cette valeur sera remplacée par le nom de l'évènement."),
    EMAIL_CONFIRMATION_LINK("Lien de confirmation de l'email", "lien de confirmation", EmailPlaceholderTypeBusinessModel.LINK, "Cette valeur sera remplacée par le lien permettant à l'utilisateur de confirmer son adresse email."),
    MAGIC_LINK("Lien de connexion magic", "lien de connexion", EmailPlaceholderTypeBusinessModel.LINK, "Cette valeur sera remplacée par le lien permettant à l'utilisateur de se connecter."),
    PASSWORD_RESET_LINK("Lien de réinitialisation de mdp", "lien de reinitialisation", EmailPlaceholderTypeBusinessModel.LINK, "Cette valeur sera remplacée par le lien permettant à l'utilisateur de réinitialiser son mot de passe."),
    TEAM_INVITATION_LINK("Lien d'invitation à une équipe", "lien invitation", EmailPlaceholderTypeBusinessModel.LINK, "Cette valeur sera remplacée par le lien permettant de rejoindre une équipe.");


    private final String name;
    private final String placeholder;
    private final EmailPlaceholderTypeBusinessModel type;
    private final String description;

    EmailPlaceholderBusinessModel(String name, String placeholder, EmailPlaceholderTypeBusinessModel type, String description) {
        this.name = name;
        this.placeholder = placeholder;
        this.type = type;
        this.description = description;
    }

    public static final List<EmailPlaceholderBusinessModel> EVENT_PLACEHOLDERS = Arrays.asList(LASTNAME, FIRSTNAME, EVENT_NAME);
    public static final List<EmailPlaceholderBusinessModel> TEAM_PLACEHOLDERS = Arrays.asList(LASTNAME, FIRSTNAME, EVENT_NAME, TEAM_NAME);
}
