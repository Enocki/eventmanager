package fr.iutdijon.eventmanager.services.email.models.business;

import fr.iutdijon.eventmanager.utils.SimpleInputDataSource;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class EmailBusinessModel {
    private List<EmailAddressBusinessModel> to = new ArrayList<>();
    private List<EmailAddressBusinessModel> cc = new ArrayList<>();
    private List<EmailAddressBusinessModel> bcc = new ArrayList<>();

    private EmailAddressBusinessModel from;
    private EmailAddressBusinessModel sender;
    private List<EmailAddressBusinessModel> replyTo = new ArrayList<>();

    private String subject;
    private String content;
    private String contentType = "text/html; charset=utf-8";

    private List<SimpleInputDataSource> attachments = new ArrayList<>();
}
