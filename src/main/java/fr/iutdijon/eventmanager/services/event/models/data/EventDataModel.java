package fr.iutdijon.eventmanager.services.event.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class EventDataModel {
    @JDapperColumnName("eventid")
    private Long id;

    @JDapperColumnName("user_userid")
    private Long ownerId;

    @JDapperColumnName("allowteamname")
    private Boolean allowTeamName;

    @JDapperColumnName("beginning")
    private java.sql.Date beginning;

    @JDapperColumnName("code")
    private String code;

    @JDapperColumnName("description")
    private String description;

    @JDapperColumnName("maximalteamsize")
    private int maximalTeamSize;

    @JDapperColumnName("minimalteamsize")
    private int minimalTeamSize;

    @JDapperColumnName("name")
    private String name;

    @JDapperColumnName("registerbeginning")
    private java.sql.Date registerBeginning;

    @JDapperColumnName("registerending")
    private java.sql.Date registerEnding;

    private List<EventDocumentRequirementDataModel> documentRequirements = new ArrayList<>();
}
