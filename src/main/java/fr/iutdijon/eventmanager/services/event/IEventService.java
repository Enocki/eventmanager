package fr.iutdijon.eventmanager.services.event;

import fr.iutdijon.eventmanager.services.event.models.business.EventEmailAddRequestBusinessModel;
import fr.iutdijon.eventmanager.services.event.models.business.*;

import java.util.List;

public interface IEventService {

    /**
     * To add an event, with calling to provider
     * @param event - Event to create
     * @return EventBusinessModel - Event created
     */
    EventBusinessModel addEvent(EventBusinessModel event);

    /**
     * To update a event.
     * @param event - Event to modify
     */
    void setEvent(EventBusinessModel event);

    /**
     * To delete an event.
     * @param eventID - Event's id to delete
     */
    void removeEvent(Long eventID);

    /**
     * To get events created by the connected moderator.
     * @return List<EventBusinessModel> of events of this modo
     */
    List<EventBusinessModel> getEventsToModerator();

    /**
     * To get events joined by the params user.
     * @return List<EventBusinessModel> of joined events
     */
    List<EventBusinessModel> getEventsJoinedByCurrentUser();

    /**
     * To get the id of joined events of the params user.
     * @param userId Long - user to get events
     * @return List<Long> of joined events
     */
    List<Long> getEventsIdJoined(Long userId);

    EventUserBusinessModel getEventCurrentUser(Long eventId);

    void setEventCurrentUser(Long eventId, EventUserBusinessModel eventUser);

    /**
     * Get the total amount of member in an event.
     * @param eventId The event id
     * @return The total amount of member in that event.
     */
    List<EventUserBusinessModel> getEventUsers(Long eventId);

    /**
     * To get the event asked by his id.
     * @param eventId Long - id
     * @return EventBusinessModel of the event
     */
    EventBusinessModel getEventById(Long eventId);

    /**
     * To get the event asked by his code.
     * @param eventCode String - code
     * @return EventBusinessModel of the event
     */
    EventBusinessModel getEventByCode(String eventCode);

    /**
     * To add a user to an event.
     * @param eventId - Event's id
     * @param userId - User's id
     */
    void addUserToEvent(Long eventId, Long userId);

    /**
     * To add the current user to an event.
     * @param eventId - Event's id
     */
    void addCurrentUserToEvent(Long eventId);

    /**
     * To remove a user to an event
     * @param eventId Long - Event's id
     * @param userId Long - User's id
     */
    void removeUserToEvent(Long eventId, Long userId);

    /**
     * To remove the current user to an event
     * @param eventId Long - Event's id
     */
    void removeCurrentUserToEvent(Long eventId);

    //#region DocumentRequirement
    EventDocumentRequirementBusinessModel getEventDocumentRequirement(Long eventDocumentRequirementId, boolean fetchContent);

    List<EventUserDocumentBusinessModel> getUserEventDocumentsFromUserIds(List<Long> userIds);
    List<EventUserDocumentBusinessModel> getCurrentUserEventDocuments(Long eventId);
    EventUserDocumentBusinessModel setCurrentUserEventDocument(EventUserDocumentBusinessModel eventUserDocument);
    //#endregion

    //#region EventEmail
    EventEmailBusinessModel addEventEmail(Long eventId, EventEmailAddRequestBusinessModel email);
    List<EventEmailBusinessModel> getEventEmails(Long eventId, EventEmailFiltersBusinessModel filters);
    //#endregion
}
