package fr.iutdijon.eventmanager.services.event.models.business;

import fr.iutdijon.eventmanager.services.event.models.data.EventUserDocumentDataModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventUserDocumentBusinessModel {
    private Long eventDocumentRequirementId;
    private Long documentId;
    private Long userId;
    private String documentMimeType;
    private String newDocumentContent;

    public EventUserDocumentDataModel toDataModel() {
        return new EventUserDocumentDataModel() {{
            setEventDocumentRequirementId(EventUserDocumentBusinessModel.this.getEventDocumentRequirementId());
            setDocumentId(EventUserDocumentBusinessModel.this.getDocumentId());
            setUserId(EventUserDocumentBusinessModel.this.getUserId());
        }};
    }

    public static EventUserDocumentBusinessModel fromDataModel(EventUserDocumentDataModel dataModel) {
        return new EventUserDocumentBusinessModel() {{
            setEventDocumentRequirementId(dataModel.getEventDocumentRequirementId());
            setDocumentId(dataModel.getDocumentId());
            setUserId(dataModel.getUserId());
        }};
    }
}
