package fr.iutdijon.eventmanager.services.event.models.business;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class EventEmailFiltersBusinessModel {
    private Date minDate;
    private Date maxDate;
}
