package fr.iutdijon.eventmanager.services.event.models.business;

import fr.iutdijon.eventmanager.services.event.models.data.EventEmailDataModel;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class EventEmailBusinessModel {
    private Long id;
    private int attachmentAmount;
    private String content;
    private Date createdAt;
    private String subject;

    public static EventEmailBusinessModel fromDataModel(EventEmailDataModel dataModel) {
        return new EventEmailBusinessModel() {{
           setId(dataModel.getId());
           setAttachmentAmount(dataModel.getAttachmentAmount());
           setContent(dataModel.getContent());
           setCreatedAt(dataModel.getCreatedAt());
           setSubject(dataModel.getSubject());
        }};
    }
}
