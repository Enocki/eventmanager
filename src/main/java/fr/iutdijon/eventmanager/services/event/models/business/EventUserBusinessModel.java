package fr.iutdijon.eventmanager.services.event.models.business;

import fr.iutdijon.eventmanager.services.event.models.data.EventUserDataModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventUserBusinessModel {
    private Long eventId;
    private Long userId;
    private String tShirtSize;

    public EventUserDataModel toDataModel() {
        return new EventUserDataModel() {{
            setEventId(EventUserBusinessModel.this.getEventId());
            setUserId(EventUserBusinessModel.this.getUserId());
            setTShirtSize(EventUserBusinessModel.this.getTShirtSize());
        }};
    }

    public static EventUserBusinessModel fromDataModel(EventUserDataModel dataModel) {
        return new EventUserBusinessModel() {{
            setEventId(dataModel.getEventId());
            setUserId(dataModel.getUserId());
            setTShirtSize(dataModel.getTShirtSize());
        }};
    }
}
