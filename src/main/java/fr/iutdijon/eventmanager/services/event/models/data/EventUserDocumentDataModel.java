package fr.iutdijon.eventmanager.services.event.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

@Getter
@Setter
public class EventUserDocumentDataModel {
    @JDapperColumnName("eventdocumentrequirement_eventdocumentrequirementid")
    private Long eventDocumentRequirementId;

    @JDapperColumnName("user_userid")
    private Long userId;

    @JDapperColumnName("document_documentid")
    private Long documentId;
}
