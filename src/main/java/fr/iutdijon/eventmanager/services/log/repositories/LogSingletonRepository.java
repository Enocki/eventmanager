package fr.iutdijon.eventmanager.services.log.repositories;

import fr.iutdijon.eventmanager.infrastructure.SingletonDatabaseConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;

@Repository
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class LogSingletonRepository implements ILogSingletonRepository {

    private final SingletonDatabaseConnection databaseConnection;

    @Autowired
    public LogSingletonRepository(SingletonDatabaseConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    @Override
    public int deleteLogs(Timestamp maximalDate) {
        return this.databaseConnection.update("DELETE FROM log WHERE calledat < ?", maximalDate);
    }
}
