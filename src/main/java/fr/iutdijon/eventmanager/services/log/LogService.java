package fr.iutdijon.eventmanager.services.log;

import fr.iutdijon.eventmanager.services.log.models.business.LogBusinessModel;
import fr.iutdijon.eventmanager.services.log.models.business.LogFiltersBusinessModel;
import fr.iutdijon.eventmanager.services.log.models.data.LogDataModel;
import fr.iutdijon.eventmanager.services.log.repositories.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

@Service
@RequestScope
public class LogService implements ILogService {
    private final LogRepository repository;

    @Autowired
    public LogService(LogRepository repository) {
        this.repository = repository;
    }

    @Override
    public void addLog(LogBusinessModel log) {
        this.repository.createLog(log.toDataModel());
    }

    @Override
    public List<LogBusinessModel> getLogs(LogFiltersBusinessModel filters) {
        List<LogDataModel> dataModels = this.repository.readLogs(filters.toDataModel());
        return dataModels.map(LogBusinessModel::fromDataModel);
    }
}
