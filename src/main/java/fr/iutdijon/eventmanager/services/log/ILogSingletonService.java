package fr.iutdijon.eventmanager.services.log;

public interface ILogSingletonService {
    void purgeLogs();
}
