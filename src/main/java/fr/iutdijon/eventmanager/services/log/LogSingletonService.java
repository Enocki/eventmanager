package fr.iutdijon.eventmanager.services.log;

import fr.iutdijon.eventmanager.services.log.repositories.ILogSingletonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class LogSingletonService implements ILogSingletonService {

    private final Logger logger = LoggerFactory.getLogger(LogSingletonService.class);

    private final ILogSingletonRepository repository;

    @Autowired
    public LogSingletonService(ILogSingletonRepository repository) {
        this.repository = repository;
    }

    @Override
    public void purgeLogs() {
        long maxDate = System.currentTimeMillis() - (60000L * 60 * 24 * 30); // 1 month
        int deletedLogs = repository.deleteLogs(new java.sql.Timestamp(maxDate));
        if (deletedLogs > 0)
            logger.info(deletedLogs + " logs purged.");
    }
}
