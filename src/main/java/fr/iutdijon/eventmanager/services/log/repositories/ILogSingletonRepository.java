package fr.iutdijon.eventmanager.services.log.repositories;

public interface ILogSingletonRepository {
    int deleteLogs(java.sql.Timestamp maximalDate);
}
