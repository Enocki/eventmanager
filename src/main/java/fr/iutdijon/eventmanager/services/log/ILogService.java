package fr.iutdijon.eventmanager.services.log;

import fr.iutdijon.eventmanager.services.log.models.business.LogBusinessModel;
import fr.iutdijon.eventmanager.services.log.models.business.LogFiltersBusinessModel;

import java.util.List;

public interface ILogService {
    void addLog(LogBusinessModel log);

    List<LogBusinessModel> getLogs(LogFiltersBusinessModel filters);
}
