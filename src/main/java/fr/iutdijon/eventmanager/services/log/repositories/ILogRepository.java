package fr.iutdijon.eventmanager.services.log.repositories;

import fr.iutdijon.eventmanager.services.log.models.data.LogDataModel;
import fr.iutdijon.eventmanager.services.log.models.data.LogFiltersDataModel;

import java.util.List;

public interface ILogRepository {

    void createLog(LogDataModel log);
    List<LogDataModel> readLogs(LogFiltersDataModel filters);
}
