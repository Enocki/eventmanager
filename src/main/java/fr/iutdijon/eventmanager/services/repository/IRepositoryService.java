package fr.iutdijon.eventmanager.services.repository;

import fr.iutdijon.eventmanager.services.repository.models.business.CityBusinessModel;
import fr.iutdijon.eventmanager.services.repository.models.business.DiplomaBusinessModel;

import java.util.List;

public interface IRepositoryService {
    CityBusinessModel getCity(int cityId);
    List<CityBusinessModel> getCities(List<Integer> cityIds);
    List<CityBusinessModel> getCities(String query);

    DiplomaBusinessModel getDiploma(int diplomaId);
    List<DiplomaBusinessModel> getDiplomas(List<Integer> diplomaIds);
    List<DiplomaBusinessModel> getDiplomas(String query);
}
