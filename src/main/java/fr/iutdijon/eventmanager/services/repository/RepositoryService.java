package fr.iutdijon.eventmanager.services.repository;

import fr.iutdijon.eventmanager.services.repository.models.business.CityBusinessModel;
import fr.iutdijon.eventmanager.services.repository.models.business.DiplomaBusinessModel;
import fr.iutdijon.eventmanager.services.repository.repositories.IRepositoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.text.Normalizer;
import java.util.Collections;
import java.util.List;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class RepositoryService implements IRepositoryService {
    private final IRepositoryRepository repository;

    @Autowired
    public RepositoryService(IRepositoryRepository repository) {
        this.repository = repository;
    }

    //#region Cities
    private List<CityBusinessModel> cities = null;
    private void checkCityCache() {
        if (cities == null)
            cities = repository.readCities().map(city -> {
                CityBusinessModel bm = CityBusinessModel.fromDataModel(city);
                bm.setNormalizedLabel(Normalizer.normalize(city.getLabel(), Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").toLowerCase());
                return bm;
            });
    }

    @Override
    public CityBusinessModel getCity(int cityId) {
        checkCityCache();
        return cities.firstOrNull(city -> city.getId() == cityId);
    }

    @Override
    public List<CityBusinessModel> getCities(List<Integer> cityIds) {
        checkCityCache();
        return cities.filter(city -> cityIds.contains(city.getId()));
    }

    @Override
    public List<CityBusinessModel> getCities(String query) {
        checkCityCache();

        query = query.trim();
        if (query.isEmpty())
            return Collections.emptyList();

        boolean isNumber = query.matches("\\d+");
        boolean isPostCode = isNumber && query.length() <= 5;

        String finalQuery = query;
        if (isPostCode)
            return cities.filter(city -> city.getPostCode().startsWith(finalQuery), 10);

        if (isNumber)
            return Collections.emptyList();

        String normalizedQuery = Normalizer.normalize(query, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").toLowerCase();
        return cities.filter(city -> city.getNormalizedLabel().startsWith(normalizedQuery), 10);
    }
    //#endregion

    //#region Diplomas
    private List<DiplomaBusinessModel> diplomas = null;
    private void checkDiplomasCache() {
        if (diplomas == null)
            diplomas = repository.readDiplomas().map(diploma -> {
                DiplomaBusinessModel bm = DiplomaBusinessModel.fromDataModel(diploma);
                bm.setNormalizedLabel(Normalizer.normalize(diploma.getLabel(), Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").toLowerCase());
                return bm;
            });
    }

    @Override
    public DiplomaBusinessModel getDiploma(int diplomaId) {
        checkDiplomasCache();
        return diplomas.firstOrNull(diploma -> diploma.getId() == diplomaId);
    }

    @Override
    public List<DiplomaBusinessModel> getDiplomas(List<Integer> diplomaIds) {
        checkDiplomasCache();
        return diplomas.filter(diploma -> diplomaIds.contains(diploma.getId()));
    }

    @Override
    public List<DiplomaBusinessModel> getDiplomas(String query) {
        checkDiplomasCache();

        query = query.trim();
        if (query.isEmpty())
            return Collections.emptyList();

        String normalizedQuery = Normalizer.normalize(query, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").toLowerCase();
        String[] requiredWords = normalizedQuery.split("\\W+");

        return diplomas.filter(diploma -> {
            for (String requiredWord : requiredWords)
                if (!diploma.getNormalizedLabel().contains(requiredWord))
                    return false;
            return true;
        }, 10);
    }
    //#endregion
}
