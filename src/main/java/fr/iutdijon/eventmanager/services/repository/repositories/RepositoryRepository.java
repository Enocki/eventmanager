package fr.iutdijon.eventmanager.services.repository.repositories;

import fr.iutdijon.eventmanager.infrastructure.SingletonDatabaseConnection;
import fr.iutdijon.eventmanager.services.repository.models.data.CityDataModel;
import fr.iutdijon.eventmanager.services.repository.models.data.DiplomaDataModel;
import net.redheademile.jdapper.JDapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class RepositoryRepository implements IRepositoryRepository {
    private final SingletonDatabaseConnection databaseConnection;

    @Autowired
    public RepositoryRepository(SingletonDatabaseConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    @Override
    public List<CityDataModel> readCities() {
        return databaseConnection.query("SELECT * FROM city ORDER BY label ASC", JDapper.getMapper(CityDataModel.class));
    }

    @Override
    public List<DiplomaDataModel> readDiplomas() {
        return databaseConnection.query("SELECT * FROM diploma ORDER BY label ASC", JDapper.getMapper(DiplomaDataModel.class));
    }
}
