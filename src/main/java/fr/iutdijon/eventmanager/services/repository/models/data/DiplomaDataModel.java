package fr.iutdijon.eventmanager.services.repository.models.data;

import lombok.Getter;
import net.redheademile.jdapper.JDapperColumnName;

@Getter
public class DiplomaDataModel {
    @JDapperColumnName("diplomaid")
    private Integer id;
    @JDapperColumnName("label")
    private String label;
}
