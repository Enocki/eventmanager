package fr.iutdijon.eventmanager.services.repository.models.business;

import fr.iutdijon.eventmanager.services.repository.models.data.CityDataModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CityBusinessModel {
    private Integer id;
    private String label;
    private String normalizedLabel;
    private String postCode;

    public static CityBusinessModel fromDataModel(CityDataModel dataModel) {
        return new CityBusinessModel() {{
            setId(dataModel.getId());
            setLabel(dataModel.getLabel());
            setPostCode(dataModel.getPostCode());
        }};
    }
}
