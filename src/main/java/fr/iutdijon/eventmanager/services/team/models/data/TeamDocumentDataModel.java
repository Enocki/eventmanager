package fr.iutdijon.eventmanager.services.team.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

@Getter
@Setter
public class TeamDocumentDataModel {
    @JDapperColumnName("eventdocumentrequirement_eventdocumentrequirementid")
    private Long eventDocumentRequirementId;

    @JDapperColumnName("team_teamid")
    private Long teamId;

    @JDapperColumnName("document_documentid")
    private Long documentId;
}
