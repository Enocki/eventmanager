package fr.iutdijon.eventmanager.services.team.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

@Getter
@Setter
public class TeamUserDataModel {
    @JDapperColumnName("user_userid")
    private Long userId;

    @JDapperColumnName("team_teamid")
    private Long teamId;
}
