package fr.iutdijon.eventmanager.services.team;

import fr.iutdijon.eventmanager.exceptions.BadRequestDetail;
import fr.iutdijon.eventmanager.services.event.IEventServiceObservable;
import fr.iutdijon.eventmanager.services.event.models.business.EventBusinessModel;
import fr.iutdijon.eventmanager.services.event.models.business.EventDocumentRequirementBusinessModel;
import fr.iutdijon.eventmanager.services.team.models.business.TeamBusinessModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import java.util.Collections;

@Component
@RequestScope
public class TeamEventServiceObservable implements IEventServiceObservable {
    private final ITeamService teamService;

    @Autowired
    public TeamEventServiceObservable(ITeamService teamService) {
        this.teamService = teamService;
    }

    @Override
    public void notifyBeforeCurrentUserLeaveEvent(long eventId) {
        TeamBusinessModel currentUserTeam = teamService.getCurrentUserTeam(eventId);
        if (currentUserTeam != null)
            teamService.removeCurrentUserFromTeam(currentUserTeam.getId());
    }

    @Override
    public void notifyEventBeforeDeletion(EventBusinessModel event) {
        teamService.removeTeams(event.getId());
    }

    @Override
    public void notifyEventDocumentRequirementBeforeDeletion(EventDocumentRequirementBusinessModel eventDocumentRequirement) {
        if (eventDocumentRequirement.isIndividual())
            return;

        teamService.removeTeamsDocument(eventDocumentRequirement.getId());
    }
}
