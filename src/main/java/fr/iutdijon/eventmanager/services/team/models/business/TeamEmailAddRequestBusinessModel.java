package fr.iutdijon.eventmanager.services.team.models.business;

import fr.iutdijon.eventmanager.utils.SimpleInputDataSource;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class TeamEmailAddRequestBusinessModel {
    private String content;
    private String subject;

    private Boolean showMyAddressAsFrom;
    private Boolean showMyAddressAsReplyTo;

    private List<SimpleInputDataSource> attachments = new ArrayList<>();
}
