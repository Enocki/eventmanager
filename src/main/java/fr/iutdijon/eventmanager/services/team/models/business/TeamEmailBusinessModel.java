package fr.iutdijon.eventmanager.services.team.models.business;

import fr.iutdijon.eventmanager.services.team.models.data.TeamEmailDataModel;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class TeamEmailBusinessModel {
    private Long id;
    private int attachmentAmount;
    private String content;
    private Date createdAt;
    private String subject;

    public static TeamEmailBusinessModel fromDataModel(TeamEmailDataModel dataModel) {
        return new TeamEmailBusinessModel() {{
            setId(dataModel.getId());
            setAttachmentAmount(dataModel.getAttachmentAmount());
            setContent(dataModel.getContent());
            setCreatedAt(dataModel.getCreatedAt());
            setSubject(dataModel.getSubject());
        }};
    }
}
