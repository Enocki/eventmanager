package fr.iutdijon.eventmanager.services.team.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

@Getter
@Setter
public class TeamInvitationDataModel {
    @JDapperColumnName("teaminvitationid")
    private Long id;

    @JDapperColumnName("user_userid")
    private Long userId;
    @JDapperColumnName("team_teamid")
    private Long teamId;
    @JDapperColumnName("recipient")
    private String recipient;
}
