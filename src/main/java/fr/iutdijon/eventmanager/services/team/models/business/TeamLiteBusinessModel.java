package fr.iutdijon.eventmanager.services.team.models.business;

import fr.iutdijon.eventmanager.services.team.models.data.TeamDataModel;
import jakarta.annotation.Nonnull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeamLiteBusinessModel {
    @Nonnull
    private Long id;
    @Nonnull
    private Long eventId;

    public static TeamLiteBusinessModel fromDataModel(TeamDataModel dataModel) {
        return new TeamLiteBusinessModel() {{
            setId(dataModel.getId());
            setEventId(dataModel.getEventId());
        }};
    }
}
