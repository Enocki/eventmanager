package fr.iutdijon.eventmanager.services.user.models.business;

import fr.iutdijon.eventmanager.services.user.models.data.UsersFiltersDataModel;
import lombok.Getter;
import lombok.Setter;

import java.util.Collections;

@Getter
@Setter
public class UsersFiltersBusinessModel {
    private String query;
    private Long requiredPermission;
    private Boolean emailConfirmed;

    private Integer limit;
    private Integer offset;

    public UsersFiltersDataModel toDataModel() {
        return new UsersFiltersDataModel() {{
            setQuery(query);
            setRequiredPermission(requiredPermission == null ? Collections.emptyList() : UserPermissionBusinessModel.fromRawPermission(requiredPermission).map(p -> p.value));
            setEmailConfirmed(emailConfirmed);
            setLimit(limit);
            setOffset(offset);
        }};
    }
}
