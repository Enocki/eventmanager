package fr.iutdijon.eventmanager.services.user.models.business;

import fr.iutdijon.eventmanager.utils.FlagUtils;

import java.util.ArrayList;
import java.util.List;

public enum UserPermissionBusinessModel {
    ADMINISTRATOR(0b1),
    MODERATOR(0b10),
    DEVELOPER(0b100);

    public final long value;
    UserPermissionBusinessModel(long value) {
        this.value = value;
    }

    public static long buildPermission(UserPermissionBusinessModel... permissions) {
        long out = 0;
        for (UserPermissionBusinessModel permission : permissions)
            out |= FlagUtils.addFlag(out, permission.value);
        return out;
    }

    public static void addPermission(UserBusinessModel user, UserPermissionBusinessModel permission) {
        user.setPermission(FlagUtils.addFlag(user.getPermission(), permission.value));
    }

    public static void removePermission(UserBusinessModel user, UserPermissionBusinessModel permission) {
        user.setPermission(FlagUtils.removeFlag(user.getPermission(), permission.value));
    }

    public static List<UserPermissionBusinessModel> fromRawPermission(long permission) {
        List<UserPermissionBusinessModel> permissions = new ArrayList<>();
        for (UserPermissionBusinessModel userPermission : UserPermissionBusinessModel.values())
            if (FlagUtils.haveFlag(permission, userPermission.value))
                permissions.add(userPermission);
        return permissions;
    }
}
