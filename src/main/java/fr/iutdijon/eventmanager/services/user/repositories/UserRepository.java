package fr.iutdijon.eventmanager.services.user.repositories;

import fr.iutdijon.eventmanager.infrastructure.RequestDatabaseConnection;
import fr.iutdijon.eventmanager.services.user.models.data.UserDataModel;
import fr.iutdijon.eventmanager.services.user.models.data.UserMagicLinkDataModel;
import fr.iutdijon.eventmanager.services.user.models.data.UsersFiltersDataModel;
import net.redheademile.jdapper.JDapper;
import org.intellij.lang.annotations.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.RequestScope;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Repository
@RequestScope
public class UserRepository implements IUserRepository {
    private final RequestDatabaseConnection databaseConnection;

    @Autowired
    public UserRepository(RequestDatabaseConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    @Override
    public UserDataModel createUser(UserDataModel user) {
        Long userId = databaseConnection.insertAndGetGeneratedLongKey(
                "INSERT INTO user (authtoken, createdat, email, emailconfirmationtoken, emailconfirmed, firstname, lastname, nextlogindate, password, permission, remainingtries, traininglabel, traininglocation, trainingyear) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                user.getAuthToken(), user.getCreatedAt(), user.getEmail(), user.getEmailConfirmationToken(), user.isEmailConfirmed(), user.getFirstName(), user.getLastName(), user.getNextLoginDate(), user.getPassword(), user.getPermission(), user.getRemainingPasswordTries(), user.getTrainingLabel(), user.getTrainingLocation(), user.getTrainingYear()
        );
        user.setId(userId);

        return user;
    }

    @Override
    public UserDataModel readUser(Long userId) {
        AtomicReference<UserDataModel> output = new AtomicReference<>();

        databaseConnection.query(
                "SELECT user.*, usermagiclink.* FROM user LEFT JOIN usermagiclink ON usermagiclink.user_userid = user.userid WHERE user.userid = ?",
                JDapper.getMapper(UserDataModel.class, UserMagicLinkDataModel.class,
                        (user, magicLink) -> {
                            if (output.get() == null)
                                output.set(user);

                            if (magicLink != null && magicLink.getId() != null)
                                output.get().getMagicLinks().add(magicLink);

                            return output.get();
                        },
                        "userid", "usermagiclinkid"),
                userId
        );

        return output.get();
    }

    @Override
    public List<UserDataModel> readUsers(List<Long> userIds) {
        if (userIds.isEmpty())
            return Collections.emptyList();

        StringBuilder builder = new StringBuilder();
        userIds.forEach(userId -> builder.append(", ").append(userId));

        return databaseConnection.query(
                "SELECT * FROM user WHERE userid IN (" + builder.substring(2) + ")",
                JDapper.getMapper(UserDataModel.class)
        );
    }

    @Override
    public List<UserDataModel> readUsers(UsersFiltersDataModel filters) {
        @Language("SQL") String sql = "SELECT * FROM user";
        List<String> whereClauses = new ArrayList<>();
        List<Object> params = new ArrayList<>();

        if (filters.getQuery() != null && !filters.getQuery().isBlank()) {
            filters.setQuery(filters.getQuery().trim().replace("%", "\\%").replace("_", "\\_"));

            whereClauses.add("(firstname LIKE ? OR lastname LIKE ? OR email LIKE ?)");
            params.add(filters.getQuery() + "%");
            params.add(filters.getQuery() + "%");
            params.add(filters.getQuery() + "%");
        }

        for (long permission : filters.getRequiredPermission()) {
            whereClauses.add("permission & ? > 0");
            params.add(permission);
        }

        if (filters.getEmailConfirmed() != null) {
            whereClauses.add("emailconfirmed = ?");
            params.add(filters.getEmailConfirmed());
        }

        if (!params.isEmpty())
            sql += " WHERE " + String.join(" AND ", whereClauses);

        sql += " ORDER BY lastname, firstname";
        sql += " LIMIT " + filters.getLimit() + " OFFSET " + filters.getOffset();

        return databaseConnection.query(sql, JDapper.getMapper(UserDataModel.class), params.toArray());
    }

    @Override
    public UserDataModel readUserByAuthToken(String authToken) {
        AtomicReference<UserDataModel> output = new AtomicReference<>();

        databaseConnection.query(
                "SELECT user.*, usermagiclink.* FROM user LEFT JOIN usermagiclink ON usermagiclink.user_userid = user.userid WHERE user.authtoken = ?",
                JDapper.getMapper(UserDataModel.class, UserMagicLinkDataModel.class,
                        (user, magicLink) -> {
                            if (output.get() == null)
                                output.set(user);

                            if (magicLink != null && magicLink.getId() != null)
                                output.get().getMagicLinks().add(magicLink);

                            return output.get();
                        },
                        "userid", "usermagiclinkid"),
                authToken
        );

        return output.get();
    }

    @Override
    public UserDataModel readUserByEmail(String email) {
        AtomicReference<UserDataModel> output = new AtomicReference<>();

        databaseConnection.query(
                "SELECT user.*, usermagiclink.* FROM user LEFT JOIN usermagiclink ON usermagiclink.user_userid = user.userid WHERE user.email = ?",
                JDapper.getMapper(UserDataModel.class, UserMagicLinkDataModel.class,
                        (user, magicLink) -> {
                            if (output.get() == null)
                                output.set(user);

                            if (magicLink != null && magicLink.getId() != null)
                                output.get().getMagicLinks().add(magicLink);

                            return output.get();
                        },
                        "userid", "usermagiclinkid"),
                email
        );

        return output.get();
    }

    @Override
    public UserDataModel readUserByEmailConfirmationToken(String emailConfirmationToken) {
        AtomicReference<UserDataModel> output = new AtomicReference<>();

        databaseConnection.query(
                "SELECT user.*, usermagiclink.* FROM user LEFT JOIN usermagiclink ON usermagiclink.user_userid = user.userid WHERE user.emailConfirmationToken = ?",
                JDapper.getMapper(UserDataModel.class, UserMagicLinkDataModel.class,
                        (user, magicLink) -> {
                            if (output.get() == null)
                                output.set(user);

                            if (magicLink != null && magicLink.getId() != null)
                                output.get().getMagicLinks().add(magicLink);

                            return output.get();
                        },
                        "userid", "usermagiclinkid"),
                emailConfirmationToken
        );

        return output.get();
    }

    @Override
    public UserDataModel readUserByMagicLinkCode(String magicLinkCode, boolean ignoreUsed) {
        AtomicReference<UserDataModel> output = new AtomicReference<>();
        String ignoreUsedClause = ignoreUsed ? " AND used = 0" : "";

        databaseConnection.query(
                "SELECT user.*, ml1.* FROM user INNER JOIN usermagiclink ON usermagiclink.user_userid = user.userid AND usermagiclink.code = ?" + ignoreUsedClause + " LEFT JOIN usermagiclink ml1 ON usermagiclink.user_userid = user.userid",
                JDapper.getMapper(UserDataModel.class, UserMagicLinkDataModel.class,
                        (user, magicLink) -> {
                            if (output.get() == null)
                                output.set(user);

                            output.get().getMagicLinks().add(magicLink);

                            return output.get();
                        },
                        "userid", "usermagiclinkid"),
                magicLinkCode
        );

        return output.get();
    }

    @Override
    public UserDataModel readUserByPasswordResetToken(String passwordResetToken) {
        AtomicReference<UserDataModel> output = new AtomicReference<>();

        databaseConnection.query(
                "SELECT user.*, usermagiclink.* FROM user LEFT JOIN usermagiclink ON usermagiclink.user_userid = user.userid WHERE user.passwordresettoken = ?",
                JDapper.getMapper(UserDataModel.class, UserMagicLinkDataModel.class,
                        (user, magicLink) -> {
                            if (output.get() == null)
                                output.set(user);

                            if (magicLink != null && magicLink.getId() != null)
                                output.get().getMagicLinks().add(magicLink);

                            return output.get();
                        },
                        "userid", "usermagiclinkid"),
                passwordResetToken
        );

        return output.get();
    }

    @Override
    public void updateUser(UserDataModel user) {
        databaseConnection.update(
                "UPDATE user SET authtoken = ?, city_cityid = ?, diploma_diplomaid = ?, document_documentid = ?, email = ?, emailconfirmationtoken = ?, emailconfirmed = ?, nextlogindate = ?, password = ?, passwordresettoken = ?, passwordresettokencreationdate = ?, permission = ?, remainingtries = ?, traininglabel = ?, traininglocation = ?, trainingyear = ? WHERE userid = ?",
                user.getAuthToken(), user.getCityId(), user.getDiplomaId(), user.getProfilePictureId(), user.getEmail(), user.getEmailConfirmationToken(), user.isEmailConfirmed(), user.getNextLoginDate(), user.getPassword(), user.getPasswordResetToken(), user.getPasswordResetTokenCreationDate(), user.getPermission(), user.getRemainingPasswordTries(), user.getTrainingLabel(), user.getTrainingLocation(), user.getTrainingYear(), user.getId()
        );
    }

    @Override
    public UserMagicLinkDataModel createUserMagicLink(UserMagicLinkDataModel magicLink) {
        Long newId = databaseConnection.insertAndGetGeneratedLongKey(
                "INSERT INTO usermagiclink (user_userid, code, createdat, used) VALUES (?, ?, ?, ?)",
                magicLink.getUserId(), magicLink.getCode(), magicLink.getCreatedAt(), magicLink.getUsed()
        );

        magicLink.setId(newId);
        return magicLink;
    }

    @Override
    public void updateUserMagicLink(String magicLinkCode, boolean used) {
        int updatedRows = databaseConnection.update("UPDATE usermagiclink SET used = ? WHERE code = ?", used, magicLinkCode);

        if (updatedRows != 1)
            throw new IllegalStateException("Not able to change magic link state");
    }
}
