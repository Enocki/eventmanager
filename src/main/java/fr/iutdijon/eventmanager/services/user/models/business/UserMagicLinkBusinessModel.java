package fr.iutdijon.eventmanager.services.user.models.business;

import fr.iutdijon.eventmanager.services.user.models.data.UserMagicLinkDataModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserMagicLinkBusinessModel {
    private Long id;
    private String code;
    private Long createdAt;
    private boolean used;

    public UserMagicLinkDataModel toDataModel() {
        return new UserMagicLinkDataModel() {{
            setId(id);
            setCode(code);
            setCreatedAt(new java.sql.Timestamp(createdAt));
            setUsed(used);
        }};
    }

    public static UserMagicLinkBusinessModel fromDataModel(UserMagicLinkDataModel dataModel) {
        return new UserMagicLinkBusinessModel() {{
            setId(dataModel.getId());
            setCode(dataModel.getCode());
            setCreatedAt(dataModel.getCreatedAt().getTime());
            setUsed(dataModel.getUsed());
        }};
    }
}
