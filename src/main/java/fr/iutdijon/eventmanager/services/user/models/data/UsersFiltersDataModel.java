package fr.iutdijon.eventmanager.services.user.models.data;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class UsersFiltersDataModel {
    private String query;
    private List<Long> requiredPermission = new ArrayList<>();
    private Boolean emailConfirmed;

    private Integer limit;
    private Integer offset;
}
