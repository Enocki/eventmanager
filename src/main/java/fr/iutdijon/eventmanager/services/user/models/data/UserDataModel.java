package fr.iutdijon.eventmanager.services.user.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class UserDataModel {
    @JDapperColumnName("userid")
    private Long id;

    @JDapperColumnName("city_cityid")
    private Integer cityId;
    @JDapperColumnName("diploma_diplomaid")
    private Integer diplomaId;
    @JDapperColumnName("document_documentid")
    private Long profilePictureId;

    @JDapperColumnName("authtoken")
    private String authToken;
    @JDapperColumnName("createdat")
    private java.sql.Timestamp createdAt;
    @JDapperColumnName("email")
    private String email;
    @JDapperColumnName("emailconfirmationtoken")
    private String emailConfirmationToken;
    @JDapperColumnName("emailconfirmed")
    private boolean emailConfirmed;
    @JDapperColumnName("firstname")
    private String firstName;
    @JDapperColumnName("lastname")
    private String lastName;
    @JDapperColumnName("nextlogindate")
    private java.sql.Timestamp nextLoginDate;
    @JDapperColumnName("password")
    private String password;
    @JDapperColumnName("passwordresettoken")
    private String passwordResetToken;
    @JDapperColumnName("passwordresettokencreationdate")
    private java.sql.Timestamp passwordResetTokenCreationDate;
    @JDapperColumnName("permission")
    private Long permission;
    @JDapperColumnName("remainingtries")
    private Short remainingPasswordTries;
    @JDapperColumnName("traininglabel")
    private String trainingLabel;
    @JDapperColumnName("traininglocation")
    private String trainingLocation;
    @JDapperColumnName("trainingyear")
    private String trainingYear;

    private List<UserMagicLinkDataModel> magicLinks = new ArrayList<>();
}
