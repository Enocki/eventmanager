package fr.iutdijon.eventmanager.services.user.models.business;

import fr.iutdijon.eventmanager.services.repository.models.business.CityBusinessModel;
import fr.iutdijon.eventmanager.services.repository.models.business.DiplomaBusinessModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserSettingsUpdateRequestBusinessModel {
    private String base64ProfilePicture;
    private String profilePictureMimeType;
    private boolean pushProfilePicture;

    private CityBusinessModel city;
    private DiplomaBusinessModel diploma;

    private String email;
    private String trainingLabel;
    private String trainingLocation;
    private String trainingYear;
}
