package fr.iutdijon.eventmanager.services.migration;

import fr.iutdijon.eventmanager.infrastructure.DatabaseTransaction;
import fr.iutdijon.eventmanager.infrastructure.SingletonDatabaseConnection;
import fr.iutdijon.eventmanager.services.migration.repositories.IMigrationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class MigrationService implements IMigrationService {

    private final Logger logger = LoggerFactory.getLogger(MigrationService.class);

    private final IMigrationRepository repository;
    private final SingletonDatabaseConnection databaseConnection;
    private final String migrationPath;

    @Autowired
    public MigrationService(
            IMigrationRepository repository,
            SingletonDatabaseConnection databaseConnection,
            String migrationPath) {
        this.repository = repository;
        this.databaseConnection = databaseConnection;
        this.migrationPath = migrationPath;
    }

    /**
     * Get all script files name
     * @return All the script files name
     */
    private List<String> getLocalScriptNames() {
        List<String> scriptNames = new ArrayList<>();
        try {
            File[] scriptFiles = ResourceUtils.getFile(this.migrationPath).listFiles();
            if (scriptFiles == null)
                return scriptNames;

            for (File file : scriptFiles)
                if (file.isFile() && file.getName().endsWith(".sql"))
                    scriptNames.add(file.getName());

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        scriptNames.sort(String::compareTo);
        return scriptNames;
    }

    @Override
    public void doMigration() {
        logger.info("Starting migration...");
        long startedAt = System.currentTimeMillis();

        List<String> localScriptNames = getLocalScriptNames();
        Set<String> passedScript = repository.readPassedScripts();

        for (String localScriptName : localScriptNames) {
            if (passedScript.contains(localScriptName))
                continue;

            try (DatabaseTransaction trx = this.databaseConnection.beginTransaction()) {
                try {
                    File scriptFile = ResourceUtils.getFile(this.migrationPath + File.separator + localScriptName);
                    String script;

                    try (FileInputStream inputStream = new FileInputStream(scriptFile)) {
                        script = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
                    } catch (Exception e) {
                        logger.error("Error when reading migration file: " + localScriptName);
                        throw new RuntimeException(e);
                    }

                    if (script.split("\n")[0].toLowerCase().contains("-- skip")) {
                        logger.info("Script execution skipped: " + localScriptName);
                        trx.rollback();
                        continue;
                    }

                    repository.playScript(script);
                    repository.createPassedScript(localScriptName);
                    logger.info("Script execution successful: " + localScriptName);
                    trx.commit();
                }
                catch (Exception e) {
                    trx.rollback();
                    logger.error("Script execution fail: " + localScriptName);
                    throw new RuntimeException(e);
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        long endedAt = System.currentTimeMillis();
        logger.info("Migration done in " + (endedAt - startedAt) + "ms!");
    }
}
