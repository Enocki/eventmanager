package fr.iutdijon.eventmanager.services.statistics.models.business;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class StatisticsEventBusinessModel {
    private Long eventId;
    private String name;
    private Long totalAmountOfMembers;
    private Long totalAmountOfMembersInTeam;
    private Long totalAmountOfTeams;
    private Long totalAmountOfTeamsValidated;
    private List<StatisticsEventTeamBusinessModel> teams;
    private List<StatisticsEventTeamMemberBusinessModel> members = new ArrayList<>();
}
