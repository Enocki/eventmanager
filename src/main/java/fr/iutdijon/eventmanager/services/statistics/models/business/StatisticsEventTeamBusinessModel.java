package fr.iutdijon.eventmanager.services.statistics.models.business;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatisticsEventTeamBusinessModel {
    private Long teamId;
    private String name;
    private boolean validated;
}
