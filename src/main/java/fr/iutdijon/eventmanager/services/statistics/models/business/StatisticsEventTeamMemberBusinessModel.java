package fr.iutdijon.eventmanager.services.statistics.models.business;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatisticsEventTeamMemberBusinessModel {
    private Long userId;

    private String firstName;
    private String lastName;

    private String cityLabel;
    private String diplomaLabel;
    private String trainingYear;

    private String tShirtSize;

    private String teamName;
}
