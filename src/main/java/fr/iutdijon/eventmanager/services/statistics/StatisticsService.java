package fr.iutdijon.eventmanager.services.statistics;

import fr.iutdijon.eventmanager.exceptions.BadRequestDetail;
import fr.iutdijon.eventmanager.services.event.IEventService;
import fr.iutdijon.eventmanager.services.event.models.business.EventBusinessModel;
import fr.iutdijon.eventmanager.services.event.models.business.EventUserBusinessModel;
import fr.iutdijon.eventmanager.services.statistics.models.business.StatisticsEventBusinessModel;
import fr.iutdijon.eventmanager.services.statistics.models.business.StatisticsEventTeamBusinessModel;
import fr.iutdijon.eventmanager.services.statistics.models.business.StatisticsEventTeamMemberBusinessModel;
import fr.iutdijon.eventmanager.services.team.ITeamService;
import fr.iutdijon.eventmanager.services.team.models.business.TeamBusinessModel;
import fr.iutdijon.eventmanager.services.user.IUserService;
import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

@Service
@RequestScope
public class StatisticsService implements IStatisticsService {
    private final IEventService eventService;
    private final ITeamService teamService;
    private final IUserService userService;

    @Autowired
    public StatisticsService(
            IEventService eventService,
            ITeamService teamService,
            IUserService userService
    ) {
        this.eventService = eventService;
        this.teamService = teamService;
        this.userService = userService;
    }

    @Override
    public byte[] getStatisticsExcelFile(Long eventId) {
        StatisticsEventBusinessModel statistics = getStatisticsEvent(eventId);

        try (XSSFWorkbook workbook = new XSSFWorkbook())
        {
            //#region Bold font
            XSSFCellStyle boldStyle = workbook.createCellStyle();
            XSSFFont boldFont = workbook.createFont();
            boldFont.setBold(true);
            boldStyle.setFont(boldFont);

            Function<Cell, Cell> toBoldCell = cell -> {
                cell.setCellStyle(boldStyle);
                return cell;
            };
            //#endregion

            //#region General Sheet
            Sheet generalSheet = workbook.createSheet("Général");

            Row nameRow = generalSheet.createRow(0);
            toBoldCell.apply(nameRow.createCell(0)).setCellValue("Nom");
            nameRow.createCell(1).setCellValue(statistics.getName());

            Row totalAmountOfMembersRow = generalSheet.createRow(1);
            toBoldCell.apply(totalAmountOfMembersRow.createCell(0)).setCellValue("Nombre total de participants");
            totalAmountOfMembersRow.createCell(1).setCellValue(statistics.getTotalAmountOfMembers());

            Row totalAmountOfMembersInTeamRow = generalSheet.createRow(2);
            toBoldCell.apply(totalAmountOfMembersInTeamRow.createCell(0)).setCellValue("Nombre de participants effectifs");
            totalAmountOfMembersInTeamRow.createCell(1).setCellValue(statistics.getTotalAmountOfMembersInTeam());

            Row totalAmountOfTeamsRow = generalSheet.createRow(3);
            toBoldCell.apply(totalAmountOfTeamsRow.createCell(0)).setCellValue("Nombre total d'équipes");
            totalAmountOfTeamsRow.createCell(1).setCellValue(statistics.getTotalAmountOfTeams());

            Row totalAmountOfTeamsValidatedRow = generalSheet.createRow(4);
            toBoldCell.apply(totalAmountOfTeamsValidatedRow.createCell(0)).setCellValue("Nombre d'équipes validées");
            totalAmountOfTeamsValidatedRow.createCell(1).setCellValue(statistics.getTotalAmountOfTeamsValidated());

            generalSheet.setColumnWidth(0, 30 * 256);
            generalSheet.setColumnWidth(1, 50 * 256);
            //#endregion

            //#region Members Sheet
            Sheet membersSheet = workbook.createSheet("Participants");

            Row headerRow = membersSheet.createRow(0);
            toBoldCell.apply(headerRow.createCell(0)).setCellValue("Nom");
            toBoldCell.apply(headerRow.createCell(1)).setCellValue("Prénom");
            toBoldCell.apply(headerRow.createCell(2)).setCellValue("Taille du T Shirt");
            toBoldCell.apply(headerRow.createCell(3)).setCellValue("Équipe");
            toBoldCell.apply(headerRow.createCell(4)).setCellValue("Ville");
            toBoldCell.apply(headerRow.createCell(5)).setCellValue("Diplôme");
            toBoldCell.apply(headerRow.createCell(6)).setCellValue("Année");

            int rowCounter = 0;
            for (StatisticsEventTeamMemberBusinessModel member : statistics.getMembers()) {
                Row memberRow = membersSheet.createRow(++rowCounter);
                memberRow.createCell(0).setCellValue(member.getFirstName());
                memberRow.createCell(1).setCellValue(member.getLastName());
                memberRow.createCell(2).setCellValue(member.getTShirtSize());
                memberRow.createCell(3).setCellValue(member.getTeamName());
                memberRow.createCell(4).setCellValue(member.getCityLabel());
                memberRow.createCell(5).setCellValue(member.getDiplomaLabel());
                memberRow.createCell(6).setCellValue(member.getTrainingYear());
            }

            membersSheet.autoSizeColumn(0);
            membersSheet.autoSizeColumn(1);
            membersSheet.autoSizeColumn(2);
            membersSheet.autoSizeColumn(3);
            membersSheet.autoSizeColumn(4);
            // Not the fifth!
            membersSheet.autoSizeColumn(6);
            //#endregion

            ByteArrayOutputStream output = new ByteArrayOutputStream();
            workbook.write(output);

            return output.toByteArray();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private StatisticsEventTeamMemberBusinessModel getStatisticsEventTeamMemberBusinessModelFromUserBusinessModel(UserBusinessModel user) {
        return new StatisticsEventTeamMemberBusinessModel() {{
            setUserId(user.getId());

            setFirstName(user.getFirstName());
            setLastName(user.getLastName());

            setCityLabel(user.getCity() != null ? user.getCity().getLabel() + " (" + user.getCity().getPostCode() + ")" : user.getTrainingLocation());
            setDiplomaLabel(user.getDiploma() != null ? user.getDiploma().getLabel() : user.getTrainingLabel());
            setTrainingYear(user.getTrainingYear());
        }};
    }

    @Override
    public StatisticsEventBusinessModel getStatisticsEvent(Long eventId) {
        EventBusinessModel event = eventService.getEventById(eventId);
        if (event == null)
            throw BadRequestDetail.UNKNOWN_EVENT.toException();

        final AtomicLong amountOfMembersInTeam = new AtomicLong(0);
        final AtomicLong amountOfTeamsValidated = new AtomicLong(0);

        List<EventUserBusinessModel> members = eventService.getEventUsers(eventId);
        List<TeamBusinessModel> teams = teamService.getTeams(eventId);

        StatisticsEventBusinessModel statistics = new StatisticsEventBusinessModel();
        statistics.setEventId(eventId);
        statistics.setName(event.getName());
        statistics.setTeams(teams.map(team -> {
            if (team.isValidated())
                amountOfTeamsValidated.incrementAndGet();

            StatisticsEventTeamBusinessModel statTeam = new StatisticsEventTeamBusinessModel();
            statTeam.setTeamId(team.getId());
            if (event.isAllowTeamName())
                statTeam.setName(team.getName());
            else
                statTeam.setName("#" + team.getId().toString());
            statTeam.setValidated(team.isValidated());

            statistics.getMembers().addAll(team.getMembers().map(member -> {
                StatisticsEventTeamMemberBusinessModel statMember = getStatisticsEventTeamMemberBusinessModelFromUserBusinessModel(member);
                statMember.setTeamName(statTeam.getName());
                return statMember;
            }));

            amountOfMembersInTeam.addAndGet(team.getMembers().size());
            return statTeam;
        }));


        List<EventUserBusinessModel> missingMemberId = members.filter(member -> !statistics.getMembers().contains(memberInTeam -> memberInTeam.getUserId().equals(member.getUserId())));
        List<UserBusinessModel> missingUsers = userService.getUsers(missingMemberId.map(EventUserBusinessModel::getUserId));

        for (EventUserBusinessModel member : members) {
            StatisticsEventTeamMemberBusinessModel stat = statistics.getMembers().singleOrNull(sMember -> sMember.getUserId().equals(member.getUserId()));
            if (stat == null) {
                stat = getStatisticsEventTeamMemberBusinessModelFromUserBusinessModel(missingUsers.single(missingUser -> missingUser.getId().equals(member.getUserId())));
                statistics.getMembers().add(stat);
            }

            stat.setTShirtSize(member.getTShirtSize());
        }

        statistics.setTotalAmountOfMembers((long) members.size());
        statistics.setTotalAmountOfMembersInTeam(amountOfMembersInTeam.get());
        statistics.setTotalAmountOfTeams((long) teams.size());
        statistics.setTotalAmountOfTeamsValidated(amountOfTeamsValidated.get());

        return statistics;
    }
}
