package fr.iutdijon.eventmanager.services.statistics;

import fr.iutdijon.eventmanager.services.statistics.models.business.StatisticsEventBusinessModel;

public interface IStatisticsService {
    /**
     * Get statistics document from a wanted events
     * @param eventId Id of the event you want to get the statistics from.
     * @return A byte array representing an Excel file
     */
    byte[] getStatisticsExcelFile(Long eventId);

    StatisticsEventBusinessModel getStatisticsEvent(Long eventId);
}
