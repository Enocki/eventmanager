package fr.iutdijon.eventmanager.services.document.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

@Getter
@Setter
public class DocumentDataModel {
    @JDapperColumnName("documentid")
    private Long id;
    @JDapperColumnName("createdat")
    private java.sql.Timestamp createdAt;
    @JDapperColumnName("name")
    private String name;
    @JDapperColumnName("type")
    private String type;

    private byte[] content;
}
