package fr.iutdijon.eventmanager.services.document.repository;

import fr.iutdijon.eventmanager.infrastructure.DatabaseTransaction;
import fr.iutdijon.eventmanager.infrastructure.RequestDatabaseConnection;
import fr.iutdijon.eventmanager.services.document.models.data.DocumentDataModel;
import net.redheademile.jdapper.JDapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.RequestScope;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

@Repository
@RequestScope
public class DocumentRepository implements IDocumentRepository {

    private final Logger logger = LoggerFactory.getLogger(DocumentRepository.class);

    private final RequestDatabaseConnection databaseConnection;
    private final File documentsDirectory;

    @Autowired
    public DocumentRepository(RequestDatabaseConnection databaseConnection, @Qualifier("documentsDirectory") File documentsDirectory) {
        this.databaseConnection = databaseConnection;
        this.documentsDirectory = documentsDirectory;

        if (!documentsDirectory.exists() && !documentsDirectory.mkdirs())
            throw new RuntimeException("Unable to create documents directory");
    }

    @Override
    public DocumentDataModel createDocument(DocumentDataModel document) {
        try (DatabaseTransaction trx = databaseConnection.beginTransaction()) {
            Long newId = databaseConnection.insertAndGetGeneratedLongKey(
                    "INSERT INTO document (createdat, name, type) VALUES (?, ?, ?)",
                    document.getCreatedAt(), document.getName(), document.getType()
            );

            document.setId(newId);

            trx.addPostCommitAction(() -> {
                try {
                    Files.write(new File(this.documentsDirectory, newId.toString()).toPath(), document.getContent());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });

            trx.commit();
            return document;
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public DocumentDataModel readDocument(Long documentId, boolean readContent) {
        List<DocumentDataModel> documents = databaseConnection.query("SELECT * FROM document WHERE documentid = ?", JDapper.getMapper(DocumentDataModel.class), documentId);

        if (documents.size() > 1)
            throw new IllegalStateException("More than one document returned");

        if (documents.isEmpty())
            return null;

        DocumentDataModel document = documents.get(0);

        if (readContent)
            try {
                document.setContent(Files.readAllBytes(new File(this.documentsDirectory, documentId.toString()).toPath()));
            }
            catch (NoSuchFileException e) { return null; }
            catch (IOException e) { throw new RuntimeException(e); }

        return document;
    }

    @Override
    public List<DocumentDataModel> readDocuments(List<Long> documentIds, boolean readContent) {
        if (documentIds.isEmpty())
            return Collections.emptyList();

        List<DocumentDataModel> documents = databaseConnection.query("SELECT * FROM document WHERE documentid IN (?" + ", ?".repeat(documentIds.size() - 1) + ")", JDapper.getMapper(DocumentDataModel.class), documentIds.toArray());

        if (readContent)
            documents.forEach(document -> {
                try {
                    document.setContent(Files.readAllBytes(new File(this.documentsDirectory, document.getId().toString()).toPath()));
                }
                catch (IOException e) { throw new RuntimeException(e); }
            });

        return documents;
    }

    @Override
    public void deleteDocument(Long documentId) {
        try (DatabaseTransaction trx = databaseConnection.beginTransaction()) {
            databaseConnection.update("DELETE FROM document WHERE documentid = ?", documentId);

            trx.addPostCommitAction(() -> {
                File document = new File(this.documentsDirectory, documentId.toString());
                if (document.exists() && !document.delete())
                    logger.warn("Unable to delete file physically: " + documentId);
            });
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteDocuments(List<Long> documentIds) {
        if (documentIds.isEmpty())
            return;

        try (DatabaseTransaction trx = databaseConnection.beginTransaction()) {
            String formattedIds = "(" + String.join(", ", documentIds.map(documentId -> documentId.toString())) + ")";

            databaseConnection.update("DELETE FROM document WHERE documentid IN " + formattedIds);

            trx.addPostCommitAction(() -> {
                for (Long documentId : documentIds) {
                    File document = new File(this.documentsDirectory, documentId.toString());
                    if (document.exists() && !document.delete())
                        logger.warn("Unable to delete file physically: " + documentId);
                }
            });
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
