package fr.iutdijon.eventmanager.services.document.models.business;

import fr.iutdijon.eventmanager.services.document.models.data.DocumentDataModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DocumentBusinessModel {
    private Long id;
    private Long createdAt;
    private String name;
    private String type;
    private String fullName;

    private byte[] content;

    public DocumentDataModel toDataModel() {
        return new DocumentDataModel() {{
            setId(id);
            setCreatedAt(new java.sql.Timestamp(createdAt));
            setName(name);
            setType(type);
            setContent(content);
        }};
    }

    public static DocumentBusinessModel fromDataModel(DocumentDataModel dataModel) {
        return new DocumentBusinessModel() {{
            setId(dataModel.getId());
            setCreatedAt(dataModel.getCreatedAt().getTime());
            setName(dataModel.getName());
            setType(dataModel.getType());
            setContent(dataModel.getContent());
        }};
    }
}
