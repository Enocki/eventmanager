package fr.iutdijon.eventmanager.services.document.repository;

import fr.iutdijon.eventmanager.services.document.models.data.DocumentDataModel;

import java.util.List;

public interface IDocumentRepository {
    DocumentDataModel createDocument(DocumentDataModel document);
    DocumentDataModel readDocument(Long documentId, boolean readContent);
    List<DocumentDataModel> readDocuments(List<Long> documentIds, boolean readContent);
    void deleteDocument(Long documentId);
    void deleteDocuments(List<Long> documentIds);
}
