package fr.iutdijon.eventmanager.services.mergingteam.repository;

import fr.iutdijon.eventmanager.services.mergingteam.models.data.TeamMergeSuggestionDataModel;

import java.util.List;

public interface IMergingTeamsRepository {
    void createTeamMergeSuggestion(TeamMergeSuggestionDataModel teamMergeSuggestion);
    List<TeamMergeSuggestionDataModel> readTeamMergeSuggestionsFromEventId(Long eventId);
    List<TeamMergeSuggestionDataModel> readTeamMergeSuggestionsFromMergingGroup(Long eventId, Long mergingGroup);
    void deleteTeamMergeSuggestionByEventId(Long eventId);
    void deleteTeamMergeSuggestionByMergingGroup(Long eventId, Long mergingGroup);
}
