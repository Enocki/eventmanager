package fr.iutdijon.eventmanager.services.mergingteam.repository;

import fr.iutdijon.eventmanager.infrastructure.RequestDatabaseConnection;
import fr.iutdijon.eventmanager.services.mergingteam.models.data.TeamMergeSuggestionDataModel;
import net.redheademile.jdapper.JDapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

@Repository
@RequestScope
public class MergingTeamsRepository implements IMergingTeamsRepository {
    private final RequestDatabaseConnection databaseConnection;

    @Autowired
    public MergingTeamsRepository(RequestDatabaseConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    @Override
    public void createTeamMergeSuggestion(TeamMergeSuggestionDataModel teamMergeSuggestion) {
        Long newId = this.databaseConnection.insertAndGetGeneratedLongKey(
                "INSERT INTO teammergesuggestion(event_eventid, team_teamid, teammerginggroup) VALUES (?, ?, ?)",
                teamMergeSuggestion.getEventId(), teamMergeSuggestion.getTeamId(), teamMergeSuggestion.getMergingGroup()
        );
        teamMergeSuggestion.setId(newId);
    }

    @Override
    public List<TeamMergeSuggestionDataModel> readTeamMergeSuggestionsFromEventId(Long eventId) {
        return this.databaseConnection.query(
                "SELECT * from teammergesuggestion WHERE event_eventid = ?",
                JDapper.getMapper(TeamMergeSuggestionDataModel.class),
                eventId
        );
    }

    @Override
    public List<TeamMergeSuggestionDataModel> readTeamMergeSuggestionsFromMergingGroup(Long eventId, Long mergingGroup) {
        return this.databaseConnection.query(
                "SELECT * from teammergesuggestion WHERE event_eventid = ? AND teammerginggroup = ?",
                JDapper.getMapper(TeamMergeSuggestionDataModel.class),
                eventId,
                mergingGroup
        );
    }

    @Override
    public void deleteTeamMergeSuggestionByEventId(Long eventId) {
        this.databaseConnection.update("DELETE FROM teammergesuggestion WHERE event_eventid = ?", eventId);
    }

    @Override
    public void deleteTeamMergeSuggestionByMergingGroup(Long eventId, Long mergingGroup) {
        this.databaseConnection.update("DELETE FROM teammergesuggestion WHERE event_eventid = ? AND teammerginggroup = ?", eventId, mergingGroup);
    }
}
