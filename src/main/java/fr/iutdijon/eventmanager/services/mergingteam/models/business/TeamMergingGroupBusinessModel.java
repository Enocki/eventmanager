package fr.iutdijon.eventmanager.services.mergingteam.models.business;

import fr.iutdijon.eventmanager.services.team.models.business.TeamBusinessModel;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class TeamMergingGroupBusinessModel {
    private Long mergingGroup;
    private List<TeamBusinessModel> teamsToMerge = new ArrayList<>();

    public int getTotalAmountOfMembers() {
        int amountOfMembers = 0;
        for (TeamBusinessModel teamBusinessModel : teamsToMerge)
            amountOfMembers += teamBusinessModel.getMembers().size();
        return amountOfMembers;
    }
}
