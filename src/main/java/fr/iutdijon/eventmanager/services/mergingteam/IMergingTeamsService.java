package fr.iutdijon.eventmanager.services.mergingteam;

import fr.iutdijon.eventmanager.services.mergingteam.models.business.TeamMergingGroupBusinessModel;

import java.util.List;

public interface IMergingTeamsService {
    List<TeamMergingGroupBusinessModel> getTeamMerging(Long eventId);
    List<TeamMergingGroupBusinessModel> generaTeamMerge(Long eventId);
    void acceptSingleTeamMerging(Long eventId, Long mergingGroup);
    void acceptAllTeamMerging(Long eventId);
    void rejectSingleTeamMerging(Long eventId, Long teamMergingId);
    void rejectAllTeamMerging(Long eventId);
}
