package fr.iutdijon.eventmanager.services.mergingteam.models.business;

import fr.iutdijon.eventmanager.services.team.models.business.TeamBusinessModel;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public class DetailedTeamBusinessModel {
    private Integer mainCityId;
    private String mainCityFallback;

    private Integer mainDiplomaId;
    private String mainDiplomaFallback;

    private TeamBusinessModel team;

    public DetailedTeamCityAndDiplomaGroupingDataBusinessModel toCityAndDiplomaGroupingData() {
        return new DetailedTeamCityAndDiplomaGroupingDataBusinessModel(mainCityId, mainCityFallback, mainDiplomaId, mainDiplomaFallback);
    }

    public DetailedTeamCityOrDiplomaGroupingDataBusinessModel toCityGroupingData() {
        return new DetailedTeamCityOrDiplomaGroupingDataBusinessModel(mainCityId, mainCityFallback);
    }

    public DetailedTeamCityOrDiplomaGroupingDataBusinessModel toDiplomaGroupingData() {
        return new DetailedTeamCityOrDiplomaGroupingDataBusinessModel(mainDiplomaId, mainDiplomaFallback);
    }

    public static class DetailedTeamCityAndDiplomaGroupingDataBusinessModel {
        private final Integer mainCityId;
        private final String mainCityFallback;

        private final Integer mainDiplomaId;
        private final String mainDiplomaFallback;

        private DetailedTeamCityAndDiplomaGroupingDataBusinessModel(Integer mainCityId, String mainCityFallback, Integer mainDiplomaId, String mainDiplomaFallback) {
            this.mainCityId = mainCityId;
            this.mainCityFallback = mainCityFallback;
            this.mainDiplomaId = mainDiplomaId;
            this.mainDiplomaFallback = mainDiplomaFallback;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DetailedTeamCityAndDiplomaGroupingDataBusinessModel that = (DetailedTeamCityAndDiplomaGroupingDataBusinessModel) o;
            return Objects.equals(mainCityId, that.mainCityId) && Objects.equals(mainCityFallback, that.mainCityFallback) && Objects.equals(mainDiplomaId, that.mainDiplomaId) && Objects.equals(mainDiplomaFallback, that.mainDiplomaFallback);
        }

        @Override
        public int hashCode() {
            return Objects.hash(mainCityId, mainCityFallback, mainDiplomaId, mainDiplomaFallback);
        }
    }

    public static class DetailedTeamCityOrDiplomaGroupingDataBusinessModel {
        private final Integer mainId;
        private final String mainFallback;

        private DetailedTeamCityOrDiplomaGroupingDataBusinessModel(Integer mainId, String mainFallback) {
            this.mainId = mainId;
            this.mainFallback = mainFallback;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DetailedTeamCityOrDiplomaGroupingDataBusinessModel that = (DetailedTeamCityOrDiplomaGroupingDataBusinessModel) o;
            return Objects.equals(mainId, that.mainId) && Objects.equals(mainFallback, that.mainFallback);
        }

        @Override
        public int hashCode() {
            return Objects.hash(mainId, mainFallback);
        }
    }
}
