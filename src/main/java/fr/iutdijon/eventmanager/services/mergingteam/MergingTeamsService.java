package fr.iutdijon.eventmanager.services.mergingteam;

import fr.iutdijon.eventmanager.exceptions.BadRequestDetail;
import fr.iutdijon.eventmanager.infrastructure.DatabaseTransaction;
import fr.iutdijon.eventmanager.infrastructure.RequestDatabaseConnection;
import fr.iutdijon.eventmanager.services.event.IEventService;
import fr.iutdijon.eventmanager.services.event.models.business.EventBusinessModel;
import fr.iutdijon.eventmanager.services.mergingteam.models.business.DetailedTeamBusinessModel;
import fr.iutdijon.eventmanager.services.mergingteam.models.business.TeamMergingGroupBusinessModel;
import fr.iutdijon.eventmanager.services.mergingteam.models.data.TeamMergeSuggestionDataModel;
import fr.iutdijon.eventmanager.services.mergingteam.repository.IMergingTeamsRepository;
import fr.iutdijon.eventmanager.services.team.ITeamService;
import fr.iutdijon.eventmanager.services.team.models.business.TeamBusinessModel;
import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Service
@RequestScope
public class MergingTeamsService implements IMergingTeamsService {
    private final ITeamService teamService;
    private final IEventService eventService;

    private final IMergingTeamsRepository repository;
    private final RequestDatabaseConnection databaseConnection;

    @Autowired
    public MergingTeamsService(
            ITeamService teamService,
            IEventService eventService,

            IMergingTeamsRepository repository,
            RequestDatabaseConnection databaseConnection
    ) {
        this.teamService = teamService;
        this.eventService = eventService;
        this.repository = repository;
        this.databaseConnection = databaseConnection;
    }

    @Override
    public List<TeamMergingGroupBusinessModel> getTeamMerging(Long eventId) {
        EventBusinessModel event = this.eventService.getEventById(eventId);
        if (event == null)
            throw BadRequestDetail.UNKNOWN_EVENT.toException();

        List<TeamMergeSuggestionDataModel> mergeSuggestions = this.repository.readTeamMergeSuggestionsFromEventId(eventId);
        List<TeamBusinessModel> requiredTeams = this.teamService.getTeams(mergeSuggestions.map(TeamMergeSuggestionDataModel::getTeamId));

        List<TeamMergingGroupBusinessModel> groups = new ArrayList<>();
        for (TeamMergeSuggestionDataModel mergeSuggestion : mergeSuggestions) {
            TeamMergingGroupBusinessModel existingGroup = groups.singleOrNull(group -> group.getMergingGroup().equals(mergeSuggestion.getMergingGroup()));
            TeamBusinessModel team = requiredTeams.single(t -> t.getId().equals(mergeSuggestion.getTeamId()));
            if (existingGroup != null) {
                existingGroup.getTeamsToMerge().add(team);
                continue;
            }

            existingGroup = new TeamMergingGroupBusinessModel();
            existingGroup.setMergingGroup(mergeSuggestion.getMergingGroup());
            existingGroup.getTeamsToMerge().add(team);
            groups.add(existingGroup);
        }

        return groups;
    }

    private void bestFitDecreasing(AlgorithmData data) {
        List<TeamMergingGroupBusinessModel> bins = new ArrayList<>();
        data.teamsToMerge.sort((t1, t2) -> Integer.compare(t2.getTeam().getMembers().size(), t1.getTeam().getMembers().size()));

        for (DetailedTeamBusinessModel team : data.teamsToMerge) {
            TeamMergingGroupBusinessModel possibleBinToFit = null;
            int possibleBestAmountMembersToFit = 0;
            for (TeamMergingGroupBusinessModel bin : bins) {
                if (bin.getTotalAmountOfMembers() + team.getTeam().getMembers().size() == data.maximalAmountOfMember) {
                    possibleBinToFit = bin;
                    break;
                }
                else if (bin.getTotalAmountOfMembers() + team.getTeam().getMembers().size() < data.maximalAmountOfMember) {
                    int possibleAmountMembers = bin.getTotalAmountOfMembers() + team.getTeam().getMembers().size();
                    if (possibleBinToFit == null || possibleBestAmountMembersToFit < possibleAmountMembers) {
                        possibleBinToFit = bin;
                        possibleBestAmountMembersToFit = possibleAmountMembers;
                    }
                }
            }

            if (possibleBinToFit != null)
                possibleBinToFit.getTeamsToMerge().add(team.getTeam());
            else
                bins.add(new TeamMergingGroupBusinessModel() {{
                    setMergingGroup(data.nextMergingGroupNumber++);
                    getTeamsToMerge().add(team.getTeam());
                }});
        }

        // Exclude not full groups
        List<TeamMergingGroupBusinessModel> notFullGroups = bins.remove(bin -> bin.getTotalAmountOfMembers() != data.maximalAmountOfMember);
        List<DetailedTeamBusinessModel> newTeamsToMerge = new ArrayList<>();
        for (TeamMergingGroupBusinessModel notFullGroup : notFullGroups)
            for (TeamBusinessModel team : notFullGroup.getTeamsToMerge())
                newTeamsToMerge.add(data.teamsToMerge.single(t -> Objects.equals(t.getTeam().getId(), team.getId())));
        data.teamsToMerge = newTeamsToMerge;

        // Insert suggestions in database
        for (TeamMergingGroupBusinessModel bin : bins)
            for (TeamBusinessModel team : bin.getTeamsToMerge())
                this.repository.createTeamMergeSuggestion(new TeamMergeSuggestionDataModel() {{
                    setMergingGroup(bin.getMergingGroup());
                    setTeamId(team.getId());
                    setEventId(data.eventId);
                }});
    }

    @Override
    public List<TeamMergingGroupBusinessModel> generaTeamMerge(Long eventId) {
        EventBusinessModel event = this.eventService.getEventById(eventId);
        if (event == null)
            throw BadRequestDetail.UNKNOWN_EVENT.toException();

        List<TeamBusinessModel> eventTeams = this.teamService.getTeams(eventId);
        List<TeamMergeSuggestionDataModel> currentSuggestions = this.repository.readTeamMergeSuggestionsFromEventId(eventId);
        AtomicLong nextMergingGroupNumber = new AtomicLong(currentSuggestions.isEmpty() ? 1 : Collections.max(currentSuggestions.map(TeamMergeSuggestionDataModel::getMergingGroup)) + 1);

        List<TeamBusinessModel> rawTeams = eventTeams.filter(team -> !currentSuggestions.contains(suggestion -> suggestion.getTeamId().equals(team.getId())));
        AtomicReference<List<DetailedTeamBusinessModel>> teamsToWorkWith = new AtomicReference<>(rawTeams.map(team -> {
            DetailedTeamBusinessModel detailedTeam = new DetailedTeamBusinessModel();
            detailedTeam.setTeam(team);

            Map<Integer, Integer> cityOccurrenceByCityId = new HashMap<>();
            Map<String, Integer> cityOccurrenceByCityFallback = new HashMap<>();
            Map<Integer, Integer> diplomaOccurrenceByDiplomaId = new HashMap<>();
            Map<String, Integer> diplomaOccurrenceByDiplomaFallback = new HashMap<>();

            for (UserBusinessModel member : team.getMembers()) {
                if (member.getCity() != null)
                    cityOccurrenceByCityId.put(member.getCity().getId(), cityOccurrenceByCityId.getOrDefault(member.getCity().getId(), 0) + 1);
                else if (!member.getTrainingLocation().isNullOrEmpty())
                    cityOccurrenceByCityFallback.put(member.getTrainingLocation(), cityOccurrenceByCityFallback.getOrDefault(member.getTrainingLocation(), 0) + 1);

                if (member.getDiploma() != null)
                    diplomaOccurrenceByDiplomaId.put(member.getDiploma().getId(), diplomaOccurrenceByDiplomaId.getOrDefault(member.getDiploma().getId(), 0) + 1);
                else if (!member.getTrainingLocation().isNullOrEmpty())
                    diplomaOccurrenceByDiplomaFallback.put(member.getTrainingLocation(), diplomaOccurrenceByDiplomaFallback.getOrDefault(member.getTrainingLocation(), 0) + 1);
            }

            Integer morePresentCityId = null;
            if (!cityOccurrenceByCityId.isEmpty())
                morePresentCityId = Collections.max(cityOccurrenceByCityId.entrySet(), Map.Entry.comparingByValue()).getKey();
            String morePresentCityFallback = null;
            if (!cityOccurrenceByCityFallback.isEmpty())
                morePresentCityFallback = Collections.max(cityOccurrenceByCityFallback.entrySet(), Map.Entry.comparingByValue()).getKey();

            Integer morePresentDiplomaId = null;
            if (!diplomaOccurrenceByDiplomaId.isEmpty())
                morePresentDiplomaId = Collections.max(diplomaOccurrenceByDiplomaId.entrySet(), Map.Entry.comparingByValue()).getKey();
            String morePresentDiplomaFallback = null;
            if (!diplomaOccurrenceByDiplomaFallback.isEmpty())
                morePresentDiplomaFallback = Collections.max(diplomaOccurrenceByDiplomaFallback.entrySet(), Map.Entry.comparingByValue()).getKey();

            if (morePresentCityId != null && morePresentCityFallback == null)
                detailedTeam.setMainCityId(morePresentCityId);
            else if (morePresentCityId == null && morePresentCityFallback != null)
                detailedTeam.setMainCityFallback(morePresentCityFallback);
            else if (morePresentCityId != null) {
                Integer idAmount = cityOccurrenceByCityId.get(morePresentCityId);
                Integer fallbackAmount = cityOccurrenceByCityFallback.get(morePresentCityFallback);

                if (idAmount >= fallbackAmount) detailedTeam.setMainCityId(morePresentCityId);
                else detailedTeam.setMainCityFallback(morePresentCityFallback);
            }

            if (morePresentDiplomaId != null && morePresentDiplomaFallback == null)
                detailedTeam.setMainDiplomaId(morePresentDiplomaId);
            else if (morePresentDiplomaId == null && morePresentDiplomaFallback != null)
                detailedTeam.setMainCityFallback(morePresentDiplomaFallback);
            else if (morePresentDiplomaId != null) {
                Integer idAmount = diplomaOccurrenceByDiplomaId.get(morePresentDiplomaId);
                Integer fallbackAmount = diplomaOccurrenceByDiplomaFallback.get(morePresentCityFallback);

                if (idAmount >= fallbackAmount) detailedTeam.setMainDiplomaId(morePresentCityId);
                else detailedTeam.setMainDiplomaFallback(morePresentDiplomaFallback);
            }

            return detailedTeam;
        }));

        try (DatabaseTransaction trx = databaseConnection.beginTransaction()) {
            Consumer<List<DetailedTeamBusinessModel>> runTheAlgorithm = teams -> {
                AlgorithmData algorithmData = new AlgorithmData();
                algorithmData.nextMergingGroupNumber = nextMergingGroupNumber.get();
                algorithmData.maximalAmountOfMember = event.getMaximalTeamSize();
                algorithmData.eventId = eventId;
                algorithmData.teamsToMerge = new ArrayList<>(teams);
                bestFitDecreasing(algorithmData);

                teamsToWorkWith.set(
                        teamsToWorkWith.get().filter(
                                t -> !teams.contains(t1 -> t1 == t) || algorithmData.teamsToMerge.contains(t1 -> t1 == t)
                        )
                );
                nextMergingGroupNumber.set(algorithmData.nextMergingGroupNumber);
            };

            // Group teams by city & diploma
            Collection<List<DetailedTeamBusinessModel>> teamsWithSameCityAndDiploma = teamsToWorkWith
                    .get()
                    .stream()
                    .filter(team -> (team.getMainCityId() != null || team.getMainCityFallback() != null) && (team.getMainDiplomaId() != null || team.getMainDiplomaFallback() != null))
                    .collect(Collectors.groupingBy(DetailedTeamBusinessModel::toCityAndDiplomaGroupingData))
                    .values();

            for (List<DetailedTeamBusinessModel> teamWithSameCityAndDiploma : teamsWithSameCityAndDiploma)
                runTheAlgorithm.accept(teamWithSameCityAndDiploma);

            // Group teams by city
            Collection<List<DetailedTeamBusinessModel>> teamsWithSameCity = teamsToWorkWith
                    .get()
                    .stream()
                    .filter(team -> team.getMainCityId() != null || team.getMainCityFallback() != null)
                    .collect(Collectors.groupingBy(DetailedTeamBusinessModel::toCityGroupingData))
                    .values();

            for (List<DetailedTeamBusinessModel> teamWithSameCity : teamsWithSameCity)
                runTheAlgorithm.accept(teamWithSameCity);

            // Group teams by diploma
            Collection<List<DetailedTeamBusinessModel>> teamsWithSameDiploma = teamsToWorkWith
                    .get()
                    .stream()
                    .filter(team -> team.getMainDiplomaId() != null || team.getMainDiplomaFallback() != null)
                    .collect(Collectors.groupingBy(DetailedTeamBusinessModel::toDiplomaGroupingData))
                    .values();

            for (List<DetailedTeamBusinessModel> teamWithSameDiploma : teamsWithSameDiploma)
                runTheAlgorithm.accept(teamWithSameDiploma);

            // All the other teams
            runTheAlgorithm.accept(teamsToWorkWith.get());

            trx.commit();

            return this.getTeamMerging(eventId);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void mergeTeams(List<TeamBusinessModel> teams) {
        String newName = String.join(" + ", teams.map(TeamBusinessModel::getName)).truncate(255);

        TeamBusinessModel newTeam = teams.getFirst();
        this.teamService.setTeam(newTeam.getId(), oldTeam -> oldTeam.setName(newName), true);

        for (TeamBusinessModel team : teams) {
            if (team == newTeam)
                continue;

            for (UserBusinessModel member : team.getMembers())
                this.teamService.addUserToTeam(member.getId(), newTeam.getId());
            this.teamService.removeTeam(team.getId());
        }
    }

    @Override
    public void acceptSingleTeamMerging(Long eventId, Long mergingGroup) {
        List<TeamMergeSuggestionDataModel> suggestions = this.repository.readTeamMergeSuggestionsFromMergingGroup(eventId, mergingGroup);
        List<TeamBusinessModel> toMerge = this.teamService.getTeams(suggestions.map(TeamMergeSuggestionDataModel::getTeamId));

        this.repository.deleteTeamMergeSuggestionByMergingGroup(eventId, mergingGroup);
        try (DatabaseTransaction trx = databaseConnection.beginTransaction()) {
            mergeTeams(toMerge);
            trx.commit();
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void acceptAllTeamMerging(Long eventId) {
        List<TeamMergingGroupBusinessModel> groups = getTeamMerging(eventId);

        this.repository.deleteTeamMergeSuggestionByEventId(eventId);
        try (DatabaseTransaction trx = databaseConnection.beginTransaction()) {
            for (TeamMergingGroupBusinessModel group : groups)
                mergeTeams(group.getTeamsToMerge());
            trx.commit();
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void rejectSingleTeamMerging(Long eventId, Long teamMergingId) {
        this.repository.deleteTeamMergeSuggestionByMergingGroup(eventId, teamMergingId);
    }

    @Override
    public void rejectAllTeamMerging(Long eventId) {
        this.repository.deleteTeamMergeSuggestionByEventId(eventId);
    }

    private static class AlgorithmData {
        private long nextMergingGroupNumber;
        private long maximalAmountOfMember;
        private long eventId;
        private List<DetailedTeamBusinessModel> teamsToMerge;
    }
}
