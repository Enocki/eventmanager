package fr.iutdijon.eventmanager.services.mergingteam.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

@Getter
@Setter
public class TeamMergeSuggestionDataModel {
    @JDapperColumnName("teammergesuggestionid")
    private Long id;

    @JDapperColumnName("event_eventid")
    private Long eventId;

    @JDapperColumnName("team_teamid")
    private Long teamId;

    @JDapperColumnName("teammerginggroup")
    private Long mergingGroup;
}
