package fr.iutdijon.eventmanager.clientapp.models.repository;

import fr.iutdijon.eventmanager.services.repository.models.business.DiplomaBusinessModel;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DiplomaViewModel {
    @NotNull
    private Integer id;
    @NotNull
    @NotEmpty
    private String label;

    public DiplomaBusinessModel toBusinessModel() {
        return new DiplomaBusinessModel() {{
            setId(DiplomaViewModel.this.getId());
            setLabel(DiplomaViewModel.this.getLabel());
        }};
    }

    public static DiplomaViewModel fromBusinessModel(DiplomaBusinessModel businessModel) {
        return new DiplomaViewModel() {{
            setId(businessModel.getId());
            setLabel(businessModel.getLabel());
        }};
    }
}
