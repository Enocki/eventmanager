package fr.iutdijon.eventmanager.clientapp.models.team;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeamInvitationStoreRequestViewModel {
    @NotNull
    @NotBlank
    @Email
    private String recipient;
}
