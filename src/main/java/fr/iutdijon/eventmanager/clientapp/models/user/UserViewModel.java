package fr.iutdijon.eventmanager.clientapp.models.user;

import fr.iutdijon.eventmanager.clientapp.models.repository.CityViewModel;
import fr.iutdijon.eventmanager.clientapp.models.repository.DiplomaViewModel;
import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserViewModel {
    @NotNull
    private Long id;

    private Long profilePictureId;

    private CityViewModel city;
    private DiplomaViewModel diploma;

    @NotNull
    private String email;
    @NotNull
    private boolean emailConfirmed;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    private Long permission;
    private String trainingLabel;
    private String trainingLocation;
    private String trainingYear;

    public static UserViewModel fromBusinessModel(UserBusinessModel businessModel) {
        return new UserViewModel() {{
            setId(businessModel.getId());
            setCity(businessModel.getCity() != null ? CityViewModel.fromBusinessModel(businessModel.getCity()) : null);
            setDiploma(businessModel.getDiploma() != null ? DiplomaViewModel.fromBusinessModel(businessModel.getDiploma()) : null);
            setProfilePictureId(businessModel.getProfilePictureId());
            setEmail(businessModel.getEmail());
            setEmailConfirmed(businessModel.isEmailConfirmed());
            setFirstName(businessModel.getFirstName());
            setLastName(businessModel.getLastName());
            setPermission(businessModel.getPermission());
            setTrainingLabel(businessModel.getTrainingLabel());
            setTrainingLocation(businessModel.getTrainingLocation());
            setTrainingYear(businessModel.getTrainingYear());
        }};
    }
}
