package fr.iutdijon.eventmanager.clientapp.models.email;

import fr.iutdijon.eventmanager.services.email.models.business.EmailBusinessModel;
import fr.iutdijon.eventmanager.services.event.models.business.EventEmailAddRequestBusinessModel;
import fr.iutdijon.eventmanager.services.team.models.business.TeamEmailAddRequestBusinessModel;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class EmailStoreRequestViewModel {
    @NotBlank
    private String content;
    @NotBlank
    private String subject;

    @NotNull
    private Boolean showMyAddressAsFrom;
    @NotNull
    private Boolean showMyAddressAsReplyTo;

    @NotNull
    public List<EmailAttachmentViewModel> attachments = new ArrayList<>();

    public List<EmailPlaceholderValueViewModel> placeholderValues;

    public EmailBusinessModel toEmailBusinessModel() {
        return new EmailBusinessModel() {{
            setContent(EmailStoreRequestViewModel.this.getContent());
            setSubject(EmailStoreRequestViewModel.this.getSubject());
            setAttachments(EmailStoreRequestViewModel.this.getAttachments().map(EmailAttachmentViewModel::toDataSource));
        }};
    }

    public EventEmailAddRequestBusinessModel toEventBusinessModel() {
        return new EventEmailAddRequestBusinessModel() {{
            setContent(EmailStoreRequestViewModel.this.getContent());
            setSubject(EmailStoreRequestViewModel.this.getSubject());
            setShowMyAddressAsFrom(EmailStoreRequestViewModel.this.getShowMyAddressAsFrom());
            setShowMyAddressAsReplyTo(EmailStoreRequestViewModel.this.getShowMyAddressAsReplyTo());
            setAttachments(EmailStoreRequestViewModel.this.getAttachments().map(EmailAttachmentViewModel::toDataSource));
        }};
    }

    public TeamEmailAddRequestBusinessModel toTeamBusinessModel() {
        return new TeamEmailAddRequestBusinessModel() {{
            setContent(EmailStoreRequestViewModel.this.getContent());
            setSubject(EmailStoreRequestViewModel.this.getSubject());
            setShowMyAddressAsFrom(EmailStoreRequestViewModel.this.getShowMyAddressAsFrom());
            setShowMyAddressAsReplyTo(EmailStoreRequestViewModel.this.getShowMyAddressAsReplyTo());
            setAttachments(EmailStoreRequestViewModel.this.getAttachments().map(EmailAttachmentViewModel::toDataSource));
        }};
    }
}
