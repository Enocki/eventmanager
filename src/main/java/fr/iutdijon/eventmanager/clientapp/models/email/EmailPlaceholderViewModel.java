package fr.iutdijon.eventmanager.clientapp.models.email;

import fr.iutdijon.eventmanager.services.email.models.business.EmailPlaceholderBusinessModel;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmailPlaceholderViewModel {
    @NotNull
    @NotBlank
    private String name;
    @NotNull
    @NotBlank
    private String placeHolder;
    @NotNull
    private EmailPlaceholderTypeViewModel type;
    @NotNull
    @NotBlank
    private String description;

    public static EmailPlaceholderViewModel fromBusinessModel(EmailPlaceholderBusinessModel businessModel) {
        return new EmailPlaceholderViewModel() {{
            setName(businessModel.getName());
            setPlaceHolder(businessModel.getPlaceholder());
            setType(EmailPlaceholderTypeViewModel.fromBusinessModel(businessModel.getType()));
            setDescription(businessModel.getDescription());
        }};
    }
}
