package fr.iutdijon.eventmanager.clientapp.models.event;

import fr.iutdijon.eventmanager.services.event.models.business.EventUserBusinessModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventUserViewModel {
    private String tShirtSize;

    public EventUserBusinessModel toBusinessModel() {
        return new EventUserBusinessModel() {{
            setTShirtSize(EventUserViewModel.this.getTShirtSize());
        }};
    }

    public static EventUserViewModel fromBusinessModel(EventUserBusinessModel businessModel) {
        return new EventUserViewModel() {{
            setTShirtSize(businessModel.getTShirtSize());
        }};
    }
}
