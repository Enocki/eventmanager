package fr.iutdijon.eventmanager.clientapp.models.user;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "discriminator")
@JsonSubTypes({
        @JsonSubTypes.Type(value = UserAuthenticationBasicRequestViewModel.class, name = "UserAuthenticationBasicRequestViewModel"),
        @JsonSubTypes.Type(value = UserAuthenticationMagicLinkRequestViewModel.class, name = "UserAuthenticationMagicLinkRequestViewModel")
})
public abstract class UserAuthenticationRequestViewModel {
}
