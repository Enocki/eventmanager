package fr.iutdijon.eventmanager.clientapp.models.team;

import fr.iutdijon.eventmanager.clientapp.models.user.UserViewModel;
import fr.iutdijon.eventmanager.services.team.models.business.TeamBusinessModel;
import fr.iutdijon.eventmanager.services.team.models.business.TeamInvitationBusinessModel;
import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeamInvitationViewModel {
    private Long id;

    private UserViewModel source;

    @NotNull
    @NotEmpty
    @Email
    private String recipient;

    public static TeamInvitationViewModel fromBusinessModel(TeamInvitationBusinessModel invitation) {
        return new TeamInvitationViewModel() {{
            setId(invitation.getId());
            setSource(UserViewModel.fromBusinessModel(invitation.getSource()));
            setRecipient(invitation.getRecipient());
        }};
    }
}
