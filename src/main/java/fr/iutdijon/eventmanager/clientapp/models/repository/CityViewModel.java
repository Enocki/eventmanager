package fr.iutdijon.eventmanager.clientapp.models.repository;

import fr.iutdijon.eventmanager.services.repository.models.business.CityBusinessModel;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CityViewModel {
    @NotNull
    private Integer id;
    @NotNull
    @NotEmpty
    private String label;
    @NotNull
    @NotEmpty
    private String postCode;

    public CityBusinessModel toBusinessModel() {
        return new CityBusinessModel() {{
            setId(CityViewModel.this.getId());
            setLabel(CityViewModel.this.getLabel());
            setPostCode(CityViewModel.this.getPostCode());
        }};
    }

    public static CityViewModel fromBusinessModel(CityBusinessModel businessModel) {
        return new CityViewModel() {{
            setId(businessModel.getId());
            setLabel(businessModel.getLabel());
            setPostCode(businessModel.getPostCode());
        }};
    }
}
