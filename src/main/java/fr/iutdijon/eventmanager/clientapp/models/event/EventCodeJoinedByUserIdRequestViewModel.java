package fr.iutdijon.eventmanager.clientapp.models.event;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventCodeJoinedByUserIdRequestViewModel {
    @NotBlank
    private String eventCode;

    private Long userId;
}

