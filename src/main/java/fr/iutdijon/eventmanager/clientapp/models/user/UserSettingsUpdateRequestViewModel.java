package fr.iutdijon.eventmanager.clientapp.models.user;

import fr.iutdijon.eventmanager.clientapp.models.repository.CityViewModel;
import fr.iutdijon.eventmanager.clientapp.models.repository.DiplomaViewModel;
import fr.iutdijon.eventmanager.services.user.models.business.UserSettingsUpdateRequestBusinessModel;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserSettingsUpdateRequestViewModel {
    private String base64ProfilePicture;
    private String profilePictureMimeType;
    @NotNull
    private boolean pushProfilePicture;

    private CityViewModel city;
    private DiplomaViewModel diploma;

    @Email
    private String email;
    private String trainingLabel;
    private String trainingLocation;
    private String trainingYear;

    public UserSettingsUpdateRequestBusinessModel toBusinessModel() {
        UserSettingsUpdateRequestBusinessModel settings = new UserSettingsUpdateRequestBusinessModel() {{
            setBase64ProfilePicture(base64ProfilePicture);
            setProfilePictureMimeType(profilePictureMimeType);
            setPushProfilePicture(pushProfilePicture);
            setEmail(email);
            setTrainingLabel(trainingLabel);
            setTrainingLocation(trainingLocation);
            setTrainingYear(trainingYear);
        }};

        if (city != null) settings.setCity(city.toBusinessModel());
        if (diploma != null) settings.setDiploma(diploma.toBusinessModel());

        return settings;
    }
}
