package fr.iutdijon.eventmanager.clientapp.models.user;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserPasswordResetCreationRequestViewModel {
    @NotBlank
    @Email
    private String email;
}
