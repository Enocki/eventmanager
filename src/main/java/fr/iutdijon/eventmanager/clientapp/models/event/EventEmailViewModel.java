package fr.iutdijon.eventmanager.clientapp.models.event;

import fr.iutdijon.eventmanager.services.event.models.business.EventEmailBusinessModel;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class EventEmailViewModel {
    @NotNull
    private Long id;
    @NotNull
    private int attachmentAmount;
    @NotBlank
    private String content;
    @NotNull
    private Date createdAt;
    @NotBlank
    private String subject;

    public static EventEmailViewModel fromBusinessModel(EventEmailBusinessModel businessModel) {
        return new EventEmailViewModel() {{
           setId(businessModel.getId());
           setAttachmentAmount(businessModel.getAttachmentAmount());
           setContent(businessModel.getContent());
           setCreatedAt(businessModel.getCreatedAt());
           setSubject(businessModel.getSubject());
        }};
    }
}
