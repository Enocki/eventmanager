package fr.iutdijon.eventmanager.clientapp.models.merging;

import fr.iutdijon.eventmanager.clientapp.models.team.TeamViewModel;
import fr.iutdijon.eventmanager.services.mergingteam.models.business.TeamMergingGroupBusinessModel;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class TeamMergeSuggestionViewModel {
    @NotNull
    private Long mergingGroup;
    @NotNull
    private List<TeamViewModel> teamsToMerge = new ArrayList<>();

    public static TeamMergeSuggestionViewModel fromBusinessModel(TeamMergingGroupBusinessModel businessModel) {
        return new TeamMergeSuggestionViewModel() {{
            setMergingGroup(businessModel.getMergingGroup());
            setTeamsToMerge(businessModel.getTeamsToMerge().map(TeamViewModel::fromBusinessModel));
        }};
    }
}
