package fr.iutdijon.eventmanager.clientapp.models.email;

import fr.iutdijon.eventmanager.services.email.models.business.EmailPlaceholderBusinessModel;
import fr.iutdijon.eventmanager.services.email.models.business.EmailTemplateBusinessModel;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
public class EmailTemplatesViewModel {
    @NotNull
    private List<EmailTemplateViewModel> templates = new ArrayList<>();
    @NotNull
    private List<EmailPlaceholderViewModel> placeholders = new ArrayList<>();

    public static EmailTemplatesViewModel fromBusinessModel(List<EmailTemplateBusinessModel> templates, EmailPlaceholderContextViewModel context) {
        List<EmailPlaceholderBusinessModel> contextualizedTemplates = switch (context) {
            case null -> Collections.emptyList();
            case ANY -> Arrays.asList(EmailPlaceholderBusinessModel.values());
            case EVENT -> EmailPlaceholderBusinessModel.EVENT_PLACEHOLDERS;
            case TEAM -> EmailPlaceholderBusinessModel.TEAM_PLACEHOLDERS;
        };

        return new EmailTemplatesViewModel() {{
            setTemplates(templates.map(EmailTemplateViewModel::fromBusinessModel));
            setPlaceholders(contextualizedTemplates.map(EmailPlaceholderViewModel::fromBusinessModel));
        }};
    }
}
