package fr.iutdijon.eventmanager.clientapp.models.event;

import fr.iutdijon.eventmanager.services.event.models.business.EventDocumentRequirementBusinessModel;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventDocumentRequirementViewModel {
    private Long id;
    private Long documentId;
    @NotNull
    private boolean individual;
    @NotNull
    @NotEmpty
    private String name;
    @NotNull
    private boolean optional;

    private String newDocumentContent;
    private String documentMimeType;

    public EventDocumentRequirementBusinessModel toBusinessModel() {
        return new EventDocumentRequirementBusinessModel() {{
            setId(EventDocumentRequirementViewModel.this.getId());
            setDocumentId(EventDocumentRequirementViewModel.this.getDocumentId());
            setIndividual(EventDocumentRequirementViewModel.this.isIndividual());
            setName(EventDocumentRequirementViewModel.this.getName());
            setOptional(EventDocumentRequirementViewModel.this.isOptional());

            setNewDocumentContent(EventDocumentRequirementViewModel.this.getNewDocumentContent());
            setDocumentMimeType(EventDocumentRequirementViewModel.this.getDocumentMimeType());
        }};
    }

    public static EventDocumentRequirementViewModel fromBusinessModel(EventDocumentRequirementBusinessModel businessModel) {
        return new EventDocumentRequirementViewModel() {{
            setId(businessModel.getId());
            setDocumentId(businessModel.getDocumentId());
            setIndividual(businessModel.isIndividual());
            setName(businessModel.getName());
            setOptional(businessModel.isOptional());

            setDocumentMimeType(businessModel.getDocumentMimeType());
        }};
    }
}
