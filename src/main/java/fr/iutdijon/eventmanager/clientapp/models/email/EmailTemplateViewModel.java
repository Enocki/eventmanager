package fr.iutdijon.eventmanager.clientapp.models.email;

import fr.iutdijon.eventmanager.services.email.models.business.EmailTemplateBusinessModel;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class EmailTemplateViewModel {
    private Long id;

    private Long ownerId;

    private String code;
    @NotBlank
    private String content;
    private String label;
    @NotBlank
    private String subject;
    @NotNull
    private Boolean shared;

    @NotNull
    private List<EmailPlaceholderViewModel> placeholders = new ArrayList<>();

    public EmailTemplateBusinessModel toBusinessModelModel() {
        return new EmailTemplateBusinessModel() {{
            setId(id);
            setOwnerId(ownerId);
            setCode(code);
            setContent(content);
            setLabel(label);
            setSubject(subject);
            setShared(shared);
        }};
    }

    public static EmailTemplateViewModel fromBusinessModel(EmailTemplateBusinessModel businessModel) {
        return new EmailTemplateViewModel() {{
            setId(businessModel.getId());
            setOwnerId(businessModel.getOwnerId());
            setCode(businessModel.getCode());
            setContent(businessModel.getContent());
            setLabel(businessModel.getLabel());
            setPlaceholders(businessModel.getPlaceholders().map(EmailPlaceholderViewModel::fromBusinessModel));
            setSubject(businessModel.getSubject());
            setShared(businessModel.getShared());
        }};
    }
}
