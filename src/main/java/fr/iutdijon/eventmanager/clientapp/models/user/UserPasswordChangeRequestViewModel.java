package fr.iutdijon.eventmanager.clientapp.models.user;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserPasswordChangeRequestViewModel {
    @NotBlank
    private String oldPassword;
    @NotBlank
    private String newPassword;
}
