package fr.iutdijon.eventmanager.clientapp.models.user;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserMagicLinkRequestViewModel {
    @NotBlank
    @Email
    private String email;

    private String redirectUrl;
}
