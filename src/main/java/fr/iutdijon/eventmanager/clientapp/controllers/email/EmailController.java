package fr.iutdijon.eventmanager.clientapp.controllers.email;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.email.EmailStoreRequestViewModel;
import fr.iutdijon.eventmanager.services.email.IEmailService;
import fr.iutdijon.eventmanager.services.email.models.business.EmailAddressBusinessModel;
import fr.iutdijon.eventmanager.services.email.models.business.EmailBusinessModel;
import fr.iutdijon.eventmanager.services.email.models.business.EmailPlaceholderBusinessModel;
import fr.iutdijon.eventmanager.services.user.IUserService;
import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
@AuthorizeAnyPolicy({AuthorizationPolicy.ADMINISTRATOR, AuthorizationPolicy.MODERATOR})
@RequestMapping("/")
public class EmailController extends EventManagerController {
    private final IEmailService emailService;
    private final IUserService userService;

    @Autowired
    public EmailController(IUserService userService, IEmailService emailService) {
        this.userService = userService;
        this.emailService = emailService;
    }

    @PostMapping("email-me")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> storeSendEmailRequest(@RequestBody @Valid EmailStoreRequestViewModel email) {
        UserBusinessModel currentUser = userService.getCurrentUser();
        EmailAddressBusinessModel addr = currentUser.toEmailAddress();

        EmailBusinessModel bm = email.toEmailBusinessModel();
        bm.setTo(Collections.singletonList(addr));

        if (email.getShowMyAddressAsFrom())
            bm.setFrom(addr);

        if (email.getShowMyAddressAsReplyTo())
            bm.setReplyTo(Collections.singletonList(addr));

        Map<EmailPlaceholderBusinessModel, String> placeholderValues = new HashMap<>();
        if (email.getPlaceholderValues() != null)
            email.getPlaceholderValues().forEach(placeholder -> placeholderValues.put(placeholder.toPlaceholder(), placeholder.getValue()));

        emailService.sendEmailAsync(bm, placeholderValues);
        return noContent();
    }
}
