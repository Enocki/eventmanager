package fr.iutdijon.eventmanager.clientapp.controllers.user;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.user.UserPasswordChangeRequestViewModel;
import fr.iutdijon.eventmanager.clientapp.models.user.UserSettingsUpdateRequestViewModel;
import fr.iutdijon.eventmanager.clientapp.models.user.UserViewModel;
import fr.iutdijon.eventmanager.services.user.IUserService;
import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AuthorizeAnyPolicy(AuthorizationPolicy.USER)
@RequestMapping("/current-user")
public class CurrentUserController extends EventManagerController {
    private final IUserService userService;

    @Autowired
    public CurrentUserController(IUserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<UserViewModel> showCurrentUser() {
        UserBusinessModel businessModel = this.userService.getCurrentUser();
        if (businessModel == null)
            throw new IllegalStateException("getCurrentUser must not return null");

        return ok(UserViewModel.fromBusinessModel(businessModel));
    }

    @PutMapping
    public ResponseEntity<UserViewModel> setCurrentUserSettings(@RequestBody @Valid UserSettingsUpdateRequestViewModel updateRequestViewModel) {
        UserBusinessModel updatedBusinessModel = userService.setCurrentUserSettings(updateRequestViewModel.toBusinessModel());
        return ok(UserViewModel.fromBusinessModel(updatedBusinessModel));
    }

    @PutMapping("password")
    public ResponseEntity<UserViewModel> setCurrentUserPassword(@RequestBody @Valid UserPasswordChangeRequestViewModel passwordChangeRequest) {
        UserBusinessModel updatedBusinessModel = this.userService.setCurrentUserPassword(passwordChangeRequest.getOldPassword(), passwordChangeRequest.getNewPassword());
        return ok(UserViewModel.fromBusinessModel(updatedBusinessModel));
    }
}
