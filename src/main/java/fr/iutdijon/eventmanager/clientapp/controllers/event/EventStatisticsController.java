package fr.iutdijon.eventmanager.clientapp.controllers.event;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.services.statistics.IStatisticsService;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AuthorizeAnyPolicy(AuthorizationPolicy.MODERATOR)
@RequestMapping("/events")
public class EventStatisticsController extends EventManagerController {
    private final IStatisticsService statisticsService;

    @Autowired
    public EventStatisticsController(IStatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    @GetMapping("{eventId}/statistics")
    public ResponseEntity<byte[]> showStatistics(@PathVariable Long eventId) {
        byte[] statistics = statisticsService.getStatisticsExcelFile(eventId);
        if (statistics == null)
            return notFound();

        return new ResponseEntity<>(
                statistics,
                new LinkedMultiValueMap<>() {{
                    add(HttpHeaders.CONTENT_TYPE, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=statistics.xlsx");
                }},
                HttpStatus.OK
        );
    }
}
