package fr.iutdijon.eventmanager.clientapp.controllers.event;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.merging.TeamMergeSuggestionViewModel;
import fr.iutdijon.eventmanager.services.mergingteam.IMergingTeamsService;
import fr.iutdijon.eventmanager.services.mergingteam.models.business.TeamMergingGroupBusinessModel;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/events")
@AuthorizeAnyPolicy(AuthorizationPolicy.MODERATOR)
public class EventTeamMergingController extends EventManagerController {
    private final IMergingTeamsService mergingTeamsService;

    @Autowired
    public EventTeamMergingController(IMergingTeamsService mergingTeamsService) {
        this.mergingTeamsService = mergingTeamsService;
    }

    @GetMapping("{eventId}/merge-suggestions")
    public ResponseEntity<List<TeamMergeSuggestionViewModel>> indexMergeSuggestion(@PathVariable Long eventId) {
        List<TeamMergingGroupBusinessModel> groups = this.mergingTeamsService.getTeamMerging(eventId);
        return ok(groups.map(TeamMergeSuggestionViewModel::fromBusinessModel));
    }

    @PostMapping("{eventId}/merge-suggestions/generate")
    public ResponseEntity<List<TeamMergeSuggestionViewModel>> storeMergeSuggestionGenerationRequest(@PathVariable Long eventId) {
        List<TeamMergingGroupBusinessModel> groups = this.mergingTeamsService.generaTeamMerge(eventId);
        return ok(groups.map(TeamMergeSuggestionViewModel::fromBusinessModel));
    }

    @PostMapping("{eventId}/merge-suggestions/{mergingGroupNumber}/accept")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> storeMergeSuggestionAcceptSingleRequest(@PathVariable Long eventId, @PathVariable Long mergingGroupNumber) {
        this.mergingTeamsService.acceptSingleTeamMerging(eventId, mergingGroupNumber);
        return noContent();
    }

    @PostMapping("{eventId}/merge-suggestions/accept-all")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> storeMergeSuggestionAcceptAllRequest(@PathVariable Long eventId) {
        this.mergingTeamsService.acceptAllTeamMerging(eventId);
        return noContent();
    }

    @PostMapping("{eventId}/merge-suggestions/{mergingGroupNumber}/reject")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> storeMergeSuggestionRejectSingleRequest(@PathVariable Long eventId, @PathVariable Long mergingGroupNumber) {
        this.mergingTeamsService.rejectSingleTeamMerging(eventId, mergingGroupNumber);
        return noContent();
    }

    @PostMapping("{eventId}/merge-suggestions/reject-all")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> storeMergeSuggestionRejectAllRequest(@PathVariable Long eventId) {
        this.mergingTeamsService.rejectAllTeamMerging(eventId);
        return noContent();
    }
}
