package fr.iutdijon.eventmanager.clientapp.controllers.user;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.user.UserMagicLinkRequestViewModel;
import fr.iutdijon.eventmanager.services.user.IUserService;
import fr.iutdijon.eventmanager.utils.AuthorizeAnonymousPolicy;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users/magic-links")
@AuthorizeAnonymousPolicy(rejectLoggedUsers = true)
public class UserMagicLinkController extends EventManagerController {
    private final IUserService userService;

    @Autowired
    public UserMagicLinkController(IUserService userService) {
        this.userService = userService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> storeUserMagicLinkRequest(@RequestBody @Valid UserMagicLinkRequestViewModel request) {
        userService.addUserMagicLink(request.getEmail(), request.getRedirectUrl());
        return noContent();
    }
}
