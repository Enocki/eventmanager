package fr.iutdijon.eventmanager.clientapp.controllers.user;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.user.UserPasswordResetCreationRequestViewModel;
import fr.iutdijon.eventmanager.clientapp.models.user.UserPasswordResetRequestViewModel;
import fr.iutdijon.eventmanager.clientapp.models.user.UserPasswordResetTokenValidityViewModel;
import fr.iutdijon.eventmanager.services.user.IUserService;
import fr.iutdijon.eventmanager.utils.AuthorizeAnonymousPolicy;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@AuthorizeAnonymousPolicy(rejectLoggedUsers = true)
public class UserPasswordController extends EventManagerController {
    private final IUserService userService;

    @Autowired
    public UserPasswordController(IUserService userService) {
        this.userService = userService;
    }

    @PostMapping("password-reset-creation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> storeUserPasswordResetCreation(@RequestBody @Valid UserPasswordResetCreationRequestViewModel passwordResetRequest) {
        this.userService.addResetUserPassword(passwordResetRequest.getEmail());
        return noContent();
    }

    @PostMapping("password-reset")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> storeUserPasswordReset(@RequestBody @Valid UserPasswordResetRequestViewModel passwordResetRequest) {
        this.userService.setUserPassword(passwordResetRequest.getPasswordResetToken(), passwordResetRequest.getNewPassword());
        return noContent();
    }

    @GetMapping("password-reset-tokens/{token}/validity")
    public ResponseEntity<UserPasswordResetTokenValidityViewModel> showPasswordResetTokenValidity(@PathVariable String token) {
        boolean valid = this.userService.isUserPasswordTokenValid(token);
        return ok(new UserPasswordResetTokenValidityViewModel() {{
            setValid(valid);
        }});
    }
}
