package fr.iutdijon.eventmanager.clientapp.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;

public abstract class EventManagerController {
    public <T> ResponseEntity<T> ok(T result) {
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public <T> ResponseEntity<T> ok() {
        return this.ok(null);
    }

    public <T> ResponseEntity<T> created(URI uri, T result) {
        return ResponseEntity.created(uri).body(result);
    }

    public <T> ResponseEntity<T> noContent() {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public <T> ResponseEntity<T> notFound(T result) {
        return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
    }

    public <T> ResponseEntity<T> notFound() {
        return this.notFound(null);
    }
}
