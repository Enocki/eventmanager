package fr.iutdijon.eventmanager.clientapp.controllers.event;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.event.EventUserViewModel;
import fr.iutdijon.eventmanager.services.event.IEventService;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/events/{eventId}/current-user")
@AuthorizeAnyPolicy(AuthorizationPolicy.USER)
public class EventUserController extends EventManagerController {
    private final IEventService eventService;

    @Autowired
    public EventUserController(IEventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping
    public ResponseEntity<EventUserViewModel> showEventUser(@PathVariable Long eventId) {
        return ok(EventUserViewModel.fromBusinessModel(this.eventService.getEventCurrentUser(eventId)));
    }

    @PutMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> updateEventUser(@PathVariable Long eventId, @RequestBody @Valid EventUserViewModel viewModel) {
        eventService.setEventCurrentUser(eventId, viewModel.toBusinessModel());
        return noContent();
    }
}
