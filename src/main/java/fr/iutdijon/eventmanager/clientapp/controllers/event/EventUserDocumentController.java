package fr.iutdijon.eventmanager.clientapp.controllers.event;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.event.EventUserDocumentViewModel;
import fr.iutdijon.eventmanager.services.event.IEventService;
import fr.iutdijon.eventmanager.services.event.models.business.EventUserDocumentBusinessModel;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/events")
@AuthorizeAnyPolicy(AuthorizationPolicy.USER)
public class EventUserDocumentController extends EventManagerController {
    private final IEventService eventService;

    @Autowired
    public EventUserDocumentController(IEventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping("{eventId}/current-user-documents")
    public ResponseEntity<List<EventUserDocumentViewModel>> indexEventCurrentUserDocuments(@PathVariable Long eventId) {
        return ok(eventService.getCurrentUserEventDocuments(eventId).map(EventUserDocumentViewModel::fromBusinessModel));
    }

    @PutMapping("{eventId}/current-user-documents")
    public ResponseEntity<EventUserDocumentViewModel> updateEventCurrentUserDocument(@PathVariable Long eventId, @RequestBody @Valid EventUserDocumentViewModel eventUserDocument) {
        EventUserDocumentBusinessModel userDocument = eventService.setCurrentUserEventDocument(eventUserDocument.toBusinessModel());
        if (userDocument == null)
            return noContent();
        return ok(EventUserDocumentViewModel.fromBusinessModel(userDocument));
    }
}
