package fr.iutdijon.eventmanager.clientapp.controllers.user;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.user.EmailConfirmationRequestViewModel;
import fr.iutdijon.eventmanager.services.user.IUserService;
import fr.iutdijon.eventmanager.utils.AuthorizeAnonymousPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users/email-confirmation")
public class UserEmailConfirmationController extends EventManagerController {
    private final IUserService userService;

    @Autowired
    public UserEmailConfirmationController(IUserService userService) {
        this.userService = userService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AuthorizeAnonymousPolicy(rejectLoggedUsers = false)
    public ResponseEntity<Void> storeEmailConfirmation(@RequestBody @Valid EmailConfirmationRequestViewModel request) {
        this.userService.setUserEmailConfirmed(request.getEmailConfirmationToken());
        return noContent();
    }

    @PostMapping("new")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AuthorizeAnyPolicy(AuthorizationPolicy.USER)
    public ResponseEntity<Void> storeNewEmailConfirmation() {
        this.userService.sendCurrentUserConfirmationEmail();
        return noContent();
    }
}
