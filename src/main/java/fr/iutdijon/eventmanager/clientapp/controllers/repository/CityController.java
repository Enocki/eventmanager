package fr.iutdijon.eventmanager.clientapp.controllers.repository;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.repository.CityViewModel;
import fr.iutdijon.eventmanager.services.repository.IRepositoryService;
import fr.iutdijon.eventmanager.utils.AuthorizeAnonymousPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cities")
@AuthorizeAnonymousPolicy(rejectLoggedUsers = false)
public class CityController extends EventManagerController {
    private final IRepositoryService repositoryService;

    @Autowired
    public CityController(IRepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }

    @GetMapping
    public ResponseEntity<List<CityViewModel>> indexCities(@RequestParam String query) {
        return ok(this.repositoryService.getCities(query).map(CityViewModel::fromBusinessModel));
    }
}
