package fr.iutdijon.eventmanager.clientapp.controllers;

import fr.iutdijon.eventmanager.clientapp.models.ErrorViewModel;
import fr.iutdijon.eventmanager.exceptions.*;
import fr.iutdijon.eventmanager.infrastructure.EventManagerHeaders;
import org.apache.catalina.connector.ClientAbortException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestValueException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.io.PrintWriter;
import java.io.StringWriter;

@ControllerAdvice
public class ErrorController extends AbstractErrorController {

    private final boolean devMode;

    @Autowired
    public ErrorController(ErrorAttributes errorAttributes, @Qualifier("devMode") boolean devMode) {
        super(errorAttributes);
        this.devMode = devMode;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorViewModel> handleException(Exception ex) {
        if (ex instanceof ClientAbortException)
            return null;

        if (ex instanceof BadRequestException badRequestException)
            return new ResponseEntity<>(
                    new ErrorViewModel() {{
                        if (badRequestException.getDetail() != null) {
                            setDetail(badRequestException.getDetail());
                            setErrorMessage(badRequestException.getDetail().errorMessage);
                        }
                        else
                            setErrorMessage(badRequestException.getMessage());
                    }},
                    HttpStatus.BAD_REQUEST
            );

        if (ex instanceof ForbiddenException)
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        if (ex instanceof TooManyRequestException tooManyRequestException)
            return new ResponseEntity<>(
                    new LinkedMultiValueMap<>() {{
                        add("Retry-After", tooManyRequestException.getRetryAfter().toString());
                    }},
                    HttpStatus.TOO_MANY_REQUESTS);

        if (ex instanceof UnauthorizedException unauthorizedException) {
            if (unauthorizedException.getRemainingLoginAttempts() != null)
                return new ResponseEntity<>(
                        new LinkedMultiValueMap<>() {{
                            add(EventManagerHeaders.REMAINING_TRIES_HEADER_NAME, unauthorizedException.getRemainingLoginAttempts().toString());
                        }},
                        HttpStatus.UNAUTHORIZED
                );

            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        if (ex instanceof MissingRequestValueException || ex instanceof MethodArgumentNotValidException || ex instanceof HttpMessageConversionException)
            return new ResponseEntity<>(new ErrorViewModel() {{
                setDetail(BadRequestDetail.INVALID_PARAMETER);
                setErrorMessage(BadRequestDetail.INVALID_PARAMETER.errorMessage);
            }}, HttpStatus.BAD_REQUEST);

        if (ex instanceof HttpRequestMethodNotSupportedException methodError)
            return new ResponseEntity<>(methodError.getHeaders(), HttpStatus.METHOD_NOT_ALLOWED);

        if (ex instanceof NoHandlerFoundException)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        if (!devMode)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw, true);
        ex.printStackTrace(pw);

        final String stackTrace = sw.getBuffer().toString();
        pw.close();

        return new ResponseEntity<>(new ErrorViewModel() {{
            setStackTrace(stackTrace);
        }}, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
