import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "../modules/shared/services/authentication.service";
import {ModalService} from "../modules/shared/services/modal.service";
import {SettingsService} from "../modules/shared/services/settings.service";
import {NavigationEnd, Router} from "@angular/router";

export type MenuItem = {
  label: string;
  route?: string;
  action?: () => void;
  exclusiveRequiredRoles?: ('administrator' | 'moderator' | 'developer')[];
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  host: {
    class: 'flex-column flex-fill'
  }
})
export class AppComponent implements OnInit {
  readonly menuItems: MenuItem[] = [
    {
      label: 'Gérer les événements',
      route: '/events',
      exclusiveRequiredRoles: ['moderator', 'developer'],
    },
    {
      label: 'Gérer les utilisateurs',
      route: '/users',
      exclusiveRequiredRoles: ['administrator', 'developer'],
    },
    {
      label: 'Gérer les emails',
      route: '/email-template',
      exclusiveRequiredRoles: ['administrator', 'developer'],
    }
  ];

  constructor(
    private readonly _authenticationService: AuthenticationService,
    private readonly _modalService: ModalService,
    private readonly _router: Router
  ) {
  }

  async logout(): Promise<void> {
    await this._authenticationService.logout();
    await this._router.navigate(['/login']);
  }

  async ngOnInit(): Promise<void> {
    await this._authenticationService.refreshUser();
    this._router.events.subscribe(val => {
      if (val instanceof NavigationEnd && this.isMenuOpened)
        this.toggleMenu();
    });
  }

  private _isMenuOpened = false;
  get isMenuOpened(): boolean {
    return this._isMenuOpened;
  }

  toggleMenu(): void {
    this._isMenuOpened = !this._isMenuOpened;
    document.body.classList.toggle("no-scrolling", this._isMenuOpened);
  }

  get isUserLogged(): boolean {
    return !!this._authenticationService.currentUser;
  }

  get isUserAdministrator(): boolean {
    return this._authenticationService.isCurrentUserAdministrator || this._authenticationService.isCurrentUserDeveloper;
  }

  get isUserModerator(): boolean {
    return this._authenticationService.isCurrentUserModerator || this._authenticationService.isCurrentUserDeveloper;
  }

  canBeShown(menuItem: MenuItem): boolean {
    if (!menuItem.exclusiveRequiredRoles)
      return true;

    for (let requiredRole of menuItem.exclusiveRequiredRoles) {
      if (requiredRole === 'administrator' && this._authenticationService.isCurrentUserAdministrator)
        return true;

      if (requiredRole === 'moderator' && this._authenticationService.isCurrentUserModerator)
        return true;

      if (requiredRole === 'developer' && this._authenticationService.isCurrentUserDeveloper)
        return true;
    }

    return false;
  }

  openQrCodeModal() {
    this._modalService.openQRCodeModal({
      code: 'coucou',
      url: 'http://localhost:4200/bonjour_les_gens'
    });
  }
}
