import {APP_INITIALIZER, ErrorHandler, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {SettingsService} from "../modules/shared/services/settings.service";
import {API_BASE_URL} from "../modules/shared/services/api.service";
import {HttpClientModule} from "@angular/common/http";
import {GlobalErrorHandler} from "../interceptor/global-error-handler";
import {SharedModule} from "../modules/shared/shared.module";
import {HomeModule} from "../modules/home/home.module";

@NgModule({
  declarations: [
    AppComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        SharedModule,
        HomeModule
    ],
  providers: [
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler
    },
    SettingsService,
    {
      provide: APP_INITIALIZER,
      useFactory: (settingsService: SettingsService) => () => settingsService.loadSettings(),
      deps: [SettingsService],
      multi: true
    },
    {
      provide: API_BASE_URL,
      useFactory: (settingsService: SettingsService) => settingsService.API_BASE_URL,
      deps: [SettingsService]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
