import {ErrorHandler, Injectable, NgZone} from "@angular/core";
import {ApiException, ErrorViewModel} from "../modules/shared/services/api.service";
import {ModalService} from "../modules/shared/services/modal.service";
import {ErrorModal} from "../modules/shared/modals/error.modal";
import {HttpErrorResponse} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class GlobalErrorHandler implements ErrorHandler {
  constructor(
    private readonly _modalService: ModalService,
    private readonly _zone: NgZone
  ) {
  }

  handleError(error: any): void {
    console.error(error);

    if (!(error instanceof HttpErrorResponse) && !ApiException.isApiException(error))
      error = error['rejection'];

    if (!!error && ApiException.isApiException(error)) {
      if (error.status === 0)
        alert('Le backoffice est injoignable');
      else if (error.status === 400 || error.status === 500)
        try {
          const errorViewModel = ErrorViewModel.fromJS(JSON.parse(error.response));
          this._zone.run(() => {
            this._modalService.openModal({
              component: ErrorModal,
              data: errorViewModel
            });
          });
        }
        catch (_) { }
    }
  }
}
