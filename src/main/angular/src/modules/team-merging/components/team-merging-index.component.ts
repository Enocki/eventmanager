import {Component, OnInit} from "@angular/core";
import {ApiService, TeamMergeSuggestionViewModel} from "../../shared/services/api.service";
import {lastValueFrom} from "rxjs";
import {ModalService} from "../../shared/services/modal.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  templateUrl: './team-merging-index.component.html',
  host: {
    class: 'flex-column gap-3 align-items-center'
  }
})
export class TeamMergingIndexComponent implements OnInit {

  eventId: number = 0;
  currentMergeSuggestions: TeamMergeSuggestionViewModel[] = [];

  constructor(
    private readonly _apiService: ApiService,
    private readonly _modalService: ModalService,
    private readonly _route: ActivatedRoute,
    private readonly _router: Router
  ) { }

  async ngOnInit(): Promise<void> {
    const eventId = this._route.snapshot.params['eventId'];
    if (!eventId || !/^\d+$/.test(eventId)) {
      await this._router.navigate(['/']);
      return;
    }

    this.eventId = Number(eventId);

    this._generateMergeRequestsLoading = true;
    await this._fetchMergeRequests();
    this._generateMergeRequestsLoading = false;
  }

  private async _fetchMergeRequests(): Promise<void> {
    this.currentMergeSuggestions = await lastValueFrom(this._apiService.indexMergeSuggestion(this.eventId));
  }

  get somethingIsLoading(): boolean {
    return this._acceptAllMergeRequestsLoading || this._rejectAllMergeRequestsLoading || this._generateMergeRequestsLoading || this._acceptMergeRequestLoading || this._rejectMergeRequestLoading;
  }

  private _acceptAllMergeRequestsLoading: boolean = false;
  async acceptAllMergeRequests(): Promise<void> {
    if (this.somethingIsLoading) return;

    this._acceptAllMergeRequestsLoading = true;
    await lastValueFrom(this._apiService.storeMergeSuggestionAcceptAllRequest(this.eventId));
    await this._fetchMergeRequests();
    this._acceptAllMergeRequestsLoading = false;
  }

  private _rejectAllMergeRequestsLoading: boolean = false;
  async rejectAllMergeRequests(): Promise<void> {
    if (this.somethingIsLoading) return;

    this._rejectAllMergeRequestsLoading = true;
    await lastValueFrom(this._apiService.storeMergeSuggestionRejectAllRequest(this.eventId));
    await this._fetchMergeRequests();
    this._rejectAllMergeRequestsLoading = false;
  }

  private _generateMergeRequestsLoading: boolean = false;
  async generateMergeRequests(): Promise<void> {
    if (this.somethingIsLoading) return;

    this._generateMergeRequestsLoading = true;
    this.currentMergeSuggestions = await lastValueFrom(this._apiService.storeMergeSuggestionGenerationRequest(this.eventId));
    this._generateMergeRequestsLoading = false;
  }

  private _acceptMergeRequestLoading: boolean = false;
  async acceptMergeRequest(mergeRequest: TeamMergeSuggestionViewModel): Promise<void> {
    if (this.somethingIsLoading) return;

    this._acceptMergeRequestLoading = true;
    await lastValueFrom(this._apiService.storeMergeSuggestionAcceptSingleRequest(this.eventId, mergeRequest.mergingGroup));
    await this._fetchMergeRequests();
    this._acceptMergeRequestLoading = false;
  }

  private _rejectMergeRequestLoading: boolean = false;
  async rejectMergeRequest(mergeRequest: TeamMergeSuggestionViewModel): Promise<void> {
    if (this.somethingIsLoading) return;

    this._rejectMergeRequestLoading = true;
    await lastValueFrom(this._apiService.storeMergeSuggestionRejectSingleRequest(this.eventId, mergeRequest.mergingGroup));
    await this._fetchMergeRequests();
    this._rejectMergeRequestLoading = false;
  }
}
