import {NgModule} from "@angular/core";
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";
import {TeamMergingIndexComponent} from "./components/team-merging-index.component";

@NgModule({
  declarations: [
    TeamMergingIndexComponent
  ],
  imports: [
    SharedModule,

    RouterModule.forChild([
      {
        path: '',
        component: TeamMergingIndexComponent,
        pathMatch: 'full'
      }
    ])
  ]
})
export class TeamMergingModule { }
