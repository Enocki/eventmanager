import {Component, OnInit} from "@angular/core";
import {ApiException, ApiService, TeamViewModel} from "../../shared/services/api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {lastValueFrom} from "rxjs";

@Component({
  templateUrl: './team-show.component.html',
  host: {
    class: 'content-width'
  },
})
export class TeamShowComponent implements OnInit {

  team: TeamViewModel = new TeamViewModel();

  constructor(
      private readonly _apiService: ApiService,
      private readonly _route: ActivatedRoute,
      private readonly _router: Router
  ) {
  }

  async ngOnInit() {
    const teamId = this._route.snapshot.params['teamId'];
    if (!teamId || !/^\d+$/.test(teamId)) {
      await this._router.navigate(['/']);
      return;
    }

    const eventId = this._route.snapshot.params['eventId'];
    if (!eventId || !/^\d+$/.test(eventId)) {
      await this._router.navigate(['/']);
      return;
    }

    try {
      this.team = await lastValueFrom(this._apiService.showTeam(Number(teamId)));
    }
    catch (error) {
      if (!(ApiException.isApiException(error)))
        throw error;

      if (error.status !== 404)
        throw error;

      await this._router.navigate(['/']);
      return;
    }

    if (Number(eventId) !== this.team.event.id)
      await this._router.navigate(['/event', this.team.event.id, 'teams', this.team.id]);
  }

}
