import {Component, OnInit} from "@angular/core";
import {ApiService, TeamViewModel} from "../../shared/services/api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {lastValueFrom} from "rxjs";
import {ModalService} from "../../shared/services/modal.service";
import {TeamDetailsModal} from "../modals/team-details.modal";

@Component({
  templateUrl: './team-index.component.html',
  host: {
    class: 'content-width'
  },
  styles: [`
    table {
      border: 1px solid black;
      border-collapse: collapse;
    }

    th, td {
      margin: 0;
      padding: .25rem;
      border-right: 1px solid black;
      border-bottom: 1px solid black;
    }

    tbody > tr:nth-child(2n + 1) {
      background-color: rgba(255, 255, 255, 0.4);
    }
  `]
})
export class TeamIndexComponent implements OnInit {
  eventId: number = -1;
  teams: TeamViewModel[] = [];

  constructor(
    private readonly _apiService: ApiService,
    private readonly _modalService: ModalService,
    private readonly _route: ActivatedRoute,
    private readonly _router: Router
  ) { }

  async ngOnInit(): Promise<void> {
    const eventId = this._route.snapshot.params['eventId'];
    if (!eventId || !/^\d+$/.test(eventId)) {
      await this._router.navigate(['/']);
      return;
    }

    this.teams = await lastValueFrom(this._apiService.indexEventTeams(this.eventId = Number(eventId)));
  }

  seeDetails(team: TeamViewModel): void {
    this._modalService.openModal({
      component: TeamDetailsModal,
      data: team,
    });
  }
}
