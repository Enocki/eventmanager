import {Modal} from "../../shared/classes/modal";
import {ApiService, TeamInvitationStoreRequestViewModel, TeamViewModel} from "../../shared/services/api.service";
import {lastValueFrom, Subject} from "rxjs";
import {Component} from "@angular/core";
import {NgForm} from "@angular/forms";

@Component({
  templateUrl: './team-invitation.modal.html',
  styles: [`
    .invisible {
      visibility: hidden;
    }

    :host {
      max-width: 100%;
      max-height: 100%;
    }
  `],
  host: {
    class: 'flex-column'
  }
})
export class TeamInvitationModal implements Modal<TeamViewModel, undefined> {
  input?: TeamViewModel;
  output!: Subject<undefined>;

  currentTab: 'qrcode' | 'email' = 'qrcode';

  invitationEmail: string = '';
  currentlyInvited: string[] = [];

  constructor(
    private readonly _apiService: ApiService
  ) {
  }

  get invitationUrl(): string {
    return window.location.origin + '/join-team/' + this.input?.code;
  }

  async invite(form: NgForm) {
    if (!form.valid || !this.input?.id)
      return;

    const invitationEmail = this.invitationEmail;
    this.invitationEmail = '';

    const newInvitation = await lastValueFrom(this._apiService.storeTeamInvitation(this.input.id, new TeamInvitationStoreRequestViewModel({
      recipient: invitationEmail
    })));
    this.input?.invitations.push(newInvitation);

    this.currentlyInvited.push(invitationEmail);
  }
}
