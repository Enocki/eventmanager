import {Component} from "@angular/core";
import {Modal} from "../../shared/classes/modal";
import {Subject} from "rxjs";

@Component({
  templateUrl: './event-join-event.modal.html',
  host: {
    class: 'position-relative flex-column align-center justify-center gap-4'
  },
  styles: [`
    :host {
      max-width: 100%;
      max-height: 100%;
    }
  `]
})
export class EventJoinEventModal implements Modal<undefined, undefined> {
  input?: undefined;
  output!: Subject<undefined>;
}
