import {Component, OnInit} from "@angular/core";
import {AuthenticationService} from "../../shared/services/authentication.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SettingsService} from "../../shared/services/settings.service";

@Component({
  templateUrl: './event-stats-moderator.component.html',
  selector: 'event-stats-moderator'
})
export class EventStatsModeratorComponent implements OnInit {

  eventId?: number;
  isModerator?: boolean = undefined;

  constructor(
    private readonly _authenticationService: AuthenticationService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _settingsService: SettingsService
  ) {
  }

  async ngOnInit(): Promise<void> {
    const eventId = this._activatedRoute.snapshot.paramMap.get('eventId');

    if (eventId == null) {
      await this._router.navigate(['../'], {relativeTo: this._activatedRoute})
      return;
    }

    const numberRegex = /^\d+$/;
    if (!numberRegex.test(eventId))
      await this._router.navigate(['../'], { relativeTo: this._activatedRoute })

    this.eventId = Number.parseInt(eventId);
    this.isModerator = this._authenticationService.isCurrentUserModerator;
  }

  downloadStatistics(): void {
    const statisticsFileUrl = this._settingsService.API_BASE_URL + '/events/' + this.eventId + '/statistics';
    window.open(statisticsFileUrl, '_blank');
  }
}
