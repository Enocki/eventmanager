import {Component, OnInit} from '@angular/core';
import {ApiService, EventViewModel} from "../../../shared/services/api.service";
import {lastValueFrom} from "rxjs";

@Component({
  selector: 'event-index-moderator',
  templateUrl: './event-index-moderator.component.html',
  host: {
    class: 'content-width'
  },
})
export class EventIndexModeratorComponent implements OnInit {

  events: EventViewModel[] = [];

  constructor(
    private readonly _apiService: ApiService) {
  }

  async ngOnInit(): Promise<void> {
    this.events = await lastValueFrom(this._apiService.indexEventsToModerator());
  }

}
