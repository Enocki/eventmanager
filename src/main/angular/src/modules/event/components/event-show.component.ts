import {Component, OnInit} from "@angular/core";
import {AuthenticationService} from "../../shared/services/authentication.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'event-show-component',
  template: `
    <span>{{eventId}}</span>
    <ng-container *ngIf="eventId !== undefined">
      <event-show-user *ngIf="!isModerator" />
      <event-stats-moderator *ngIf="isModerator" />
    </ng-container>
  `
})
export class EventShowComponent implements OnInit {
  eventId?: number;
  isModerator?: boolean = undefined;

  constructor(
    private readonly _authenticationService: AuthenticationService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _router: Router,
  ) {
  }

  async ngOnInit(): Promise<void> {
    const eventId = this._activatedRoute.snapshot.paramMap.get('eventId');

    if (eventId == null) {
      await this._router.navigate(['../'], {relativeTo: this._activatedRoute})
      return;
    }

    const numberRegex = /^\d+$/;
    if (!numberRegex.test(eventId))
      await this._router.navigate(['../'], { relativeTo: this._activatedRoute })

    this.eventId = Number.parseInt(eventId);
    this.isModerator = this._authenticationService.isCurrentUserModerator;
  }
}
