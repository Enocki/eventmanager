import {Component, OnInit} from "@angular/core";
import {ApiService, EventEmailViewModel, PlaceholderContextViewModel} from "../../shared/services/api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {lastValueFrom} from "rxjs";
import {ModalService} from "../../shared/services/modal.service";

@Component({
  templateUrl: './event-email.component.html',
  host: {
    class: 'content-width'
  },
  styles: [`
    table {
      border: 1px solid black;
      border-collapse: collapse;
    }

    th, td {
      margin: 0;
      padding: .25rem;
      border-right: 1px solid black;
      border-bottom: 1px solid black;
    }

    tbody > tr:nth-child(2n + 1) {
      background-color: rgba(255, 255, 255, 0.4);
    }
  `]
})
export class EventEmailComponent implements OnInit {
  eventId: number = -1;
  emails: EventEmailViewModel[] = [];

  constructor(
    private readonly _apiService: ApiService,
    private readonly _modalService: ModalService,
    private readonly _route: ActivatedRoute,
    private readonly _router: Router
  ) {
  }

  async ngOnInit(): Promise<void> {
    const eventId = this._route.snapshot.params['eventId'];
    if (!eventId || !/^\d+$/.test(eventId)) {
      await this._router.navigate(['/']);
      return;
    }

    this.eventId = Number(eventId);
    this.emails = await lastValueFrom(this._apiService.indexEventEmails(this.eventId, undefined, undefined));
  }

  writeNewEmail() {
    this._modalService.openEmailRedactionModal( PlaceholderContextViewModel.EVENT, async email => {
      if (!email)
        return;

      const historizedEmail = await lastValueFrom(this._apiService.storeEventEmail(this.eventId, email));
      this.emails = [...this.emails, historizedEmail];
    });
  }

  seeEmailContent(email: EventEmailViewModel) {
    this._modalService.openEmailContentModal(email.content);
  }
}
