import {Component} from "@angular/core";
import {UserRegistrationRequestViewModel} from "../../shared/services/api.service";
import {AuthenticationService} from "../../shared/services/authentication.service";
import {ActivatedRoute, Params, Router} from "@angular/router";

@Component({
  templateUrl: './register-index.component.html',
  host: {
    class: 'content-width'
  },
})
export class RegisterIndexComponent {

  email: string = "";
  firstName: string = "";
  lastName: string = "";
  password: string = "";
  passwordconfirmation: string = "";

  constructor(
    private readonly _authenticationService: AuthenticationService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _router: Router
  ) { }

  private get _redirectUrl(): string | undefined {
    const redirectUrl = this._activatedRoute.snapshot.queryParams['redirect-url'];
    return !!redirectUrl ? redirectUrl : undefined;
  }

  get loginQueryParams(): Params {
    const redirectUrl = this._redirectUrl;
    if (!!redirectUrl)
      return { 'redirect-url': redirectUrl };
    return {};
  }

  get passwordSecurityLevel(): string {
    const longEnough = this.password.length > 8;
    if (!longEnough)
      return "Très faible";

    const hasLowerCharacter = /[a-z]/.test(this.password);
    const hasUpperCharacter = /[A-Z]/.test(this.password);
    const hasNumber = /[0-9]/.test(this.password);
    const hasSpecialCharacter = /[^0-9a-zA-Z]/.test(this.password);

    return ["Très faible", "Faible", "Moyen", "Fort", "Très fort"][+hasLowerCharacter + +hasUpperCharacter + +hasNumber + +hasSpecialCharacter];
  }

  async submit() {
    if (this.password !== this.passwordconfirmation)
      return;

    await this._authenticationService.register(new UserRegistrationRequestViewModel({
      email: this.email,
      firstName: this.firstName,
      lastName: this.lastName,
      password: this.password
    }));

    const redirectUrl = this._redirectUrl;
    await this._router.navigate([!!redirectUrl ? redirectUrl : '/']);
  }
}
