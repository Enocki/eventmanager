import {NgModule} from "@angular/core";
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";
import {PasswordResetIndexComponent} from "./components/password-reset-index.component";

@NgModule({
  declarations: [
    PasswordResetIndexComponent
  ],
  imports: [
    SharedModule,

    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        component: PasswordResetIndexComponent
      }
    ])
  ]
})
export class ResetPasswordModule { }
