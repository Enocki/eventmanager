import {Component, OnInit, ViewChild} from "@angular/core";
import {
  ApiException,
  ApiService,
  CityViewModel,
  DiplomaViewModel,
  ErrorViewModelDetail,
  UserPasswordChangeRequestViewModel,
  UserSettingsUpdateRequestViewModel
} from "../../shared/services/api.service";
import {AuthenticationService} from "../../shared/services/authentication.service";
import {lastValueFrom} from "rxjs";
import {BadRequestUtils} from "../../shared/classes/badrequest-utils";
import {NgForm} from "@angular/forms";

@Component({
  templateUrl: './settings-index.component.html',
  styleUrls: ['./settings-index.less'],
  host: {
    class: 'content-width'
  },
})
export class SettingsIndexComponent implements OnInit {

  @ViewChild('form', { static: true }) settingsForm!: NgForm;

  settings: UserSettingsUpdateRequestViewModel = new UserSettingsUpdateRequestViewModel({ pushProfilePicture: false });
  passwordChangeRequest: UserPasswordChangeRequestViewModel = new UserPasswordChangeRequestViewModel( { newPassword: '', oldPassword: '' });
  confirmPassword: string = '';

  constructor(
    private readonly _apiService: ApiService,
    private readonly _authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this._initSettings();
  }

  private _initSettings() {
    this.settings = new UserSettingsUpdateRequestViewModel({ pushProfilePicture: false });

    this.settings.city = this._authenticationService.currentUser?.city;
    this.settings.diploma = this._authenticationService.currentUser?.diploma;

    this.settings.email = this._authenticationService.currentUser?.email;
    this.settings.trainingLabel = this._authenticationService.currentUser?.trainingLabel;
    this.settings.trainingLocation = this._authenticationService.currentUser?.trainingLocation;
    this.settings.trainingYear = this._authenticationService.currentUser?.trainingYear;

    if (!!this._authenticationService.currentUser?.trainingLocation) {
      this._trainingLocationAutoComplete = `--- Autre ---`;
      this.otherLocationSelected = true;
    }
    else if (!!this._authenticationService.currentUser?.city)
      this._trainingLocationAutoComplete = `${this._authenticationService.currentUser.city.label} (${this._authenticationService.currentUser.city.postCode})`;

    if (!!this._authenticationService.currentUser?.trainingLabel) {
      this._diplomaAutoComplete = '--- Autre ---';
      this.otherDiplomaSelected = true;
    }
    else if (!!this._authenticationService.currentUser?.diploma)
      this._diplomaAutoComplete = this._authenticationService.currentUser.diploma.label;
  }

  get currentProfilePictureId(): number | undefined {
    return this._authenticationService.currentUser?.profilePictureId;
  }

  get isCurrentUserEmailConfirmed(): boolean {
    return this._authenticationService.currentUser?.emailConfirmed ?? false;
  }

  private _markSettingsFormAsDirty() {
    this.settingsForm.form.markAsDirty();
  }

  //#region Profile Picture
  onFileSelected(event: Event) {
    if (!(event.target instanceof HTMLInputElement))
      return;

    if (!event.target.files)
      return;

    const file = event.target.files[0];
    this.settings.pushProfilePicture = true;
    this.settings.profilePictureMimeType = file.type;

    const reader = new FileReader();
    reader.onload = () => {
      this.settings.base64ProfilePicture = (reader.result as string).split(',')[1];
      this._markSettingsFormAsDirty();
    };
    reader.readAsDataURL(file);
  }

  deleteCurrentProfilePicture() {
    this.settings.pushProfilePicture = true;
    this.settings.profilePictureMimeType = undefined;
    this.settings.base64ProfilePicture = undefined;
    this._markSettingsFormAsDirty();
  }

  cancelProfilePictureEdition() {
    this.settings.pushProfilePicture = false;
    this.settings.profilePictureMimeType = undefined;
    this.settings.base64ProfilePicture = undefined;
  }

  get isNewProfilePictureSelected(): boolean {
    return this.settings.pushProfilePicture && this.settings.base64ProfilePicture !== undefined;
  }

  get canCurrentProfilePictureBeDeleted(): boolean {
    return !!this.currentProfilePictureId && (!this.settings.pushProfilePicture || this.settings.base64ProfilePicture !== undefined);
  }

  get canProfilePictureEditionBeCancelled(): boolean {
    return this.settings.pushProfilePicture;
  }
  //#endregion

  async submit(): Promise<void> {
    try {
      await this._authenticationService.setSettings(this.settings);
      this._initSettings();
      this.settingsForm.form.markAsPristine();
      alert('Paramètre enregistré!');
    }
    catch (e) {
      const error = BadRequestUtils.asBadRequestException(e);
      if (error.detail === ErrorViewModelDetail.DOCUMENT_TOO_BIG)
        alert('Le document est trop gros en fait!');
      else
        throw e;
    }
  }

  async sendEmailConfirmation(): Promise<void> {
    await lastValueFrom(this._apiService.storeNewEmailConfirmation());
  }

  async submitPasswordChange(): Promise<void> {
    if (this.passwordChangeRequest.oldPassword.length === 0
      || this.passwordChangeRequest.newPassword.length === 0
      || this.passwordChangeRequest.newPassword !== this.confirmPassword)
      return;

    try {
      await lastValueFrom(this._apiService.setCurrentUserPassword(this.passwordChangeRequest));
      this.passwordChangeRequest = new UserPasswordChangeRequestViewModel( { newPassword: '', oldPassword: '' });
      this.confirmPassword = '';
      alert('Nouveau mot de passe enregistré!');
    }
    catch (e) {
      if (!ApiException.isApiException(e) || e.status !== 401) throw e;
      alert('Mauvais ancien mot de passe');
    }
  }

  //#region Autocomplete

  //#region Location
  private _trainingLocationAutoComplete?: string;
  otherLocationSelected: boolean = false;
  get trainingLocationAutoComplete(): string | undefined {
    return this._trainingLocationAutoComplete;
  }

  private locationFetchTimeout: any;
  locationSuggestions?: CityViewModel[] = undefined;
  locationSelectedSuggestionIndex: number = 0;

  set trainingLocationAutoComplete(value: string | undefined) {
    this._trainingLocationAutoComplete = value;

    clearTimeout(this.locationFetchTimeout);
    this.locationFetchTimeout = setTimeout(async () => {
      this.locationSelectedSuggestionIndex = 0;
      if (!!value)
        this.locationSuggestions = await lastValueFrom(this._apiService.indexCities(value));
      else
        this.locationSuggestions = [];
    }, 200);
  }

  onLocationKeyPressed(event: KeyboardEvent) {
    if (event.key.toLowerCase() === 'arrowup') {
      if (--this.locationSelectedSuggestionIndex < 0 && this.locationSuggestions !== undefined)
        this.locationSelectedSuggestionIndex = this.locationSuggestions.length;
    }
    else if (event.key.toLowerCase() === 'arrowdown') {
      if (this.locationSuggestions !== undefined && ++this.locationSelectedSuggestionIndex > this.locationSuggestions.length)
        this.locationSelectedSuggestionIndex = 0;
    }
    else if (event.key.toLowerCase() === 'escape')
      this.locationSuggestions = [];
    else if (event.key.toLowerCase() === 'enter' && this.locationSuggestions !== undefined) {
      if (this.locationSelectedSuggestionIndex === this.locationSuggestions.length)
        this.selectOtherLocation();
      else
        this.selectLocation(this.locationSuggestions[this.locationSelectedSuggestionIndex]);
    }
    else
      return;

    event.preventDefault();
    event.stopPropagation();
    event.stopImmediatePropagation();
  }

  onBlurLocation() {
    clearTimeout(this.locationFetchTimeout);
    if (!this._trainingLocationAutoComplete) {
      this._trainingLocationAutoComplete = undefined;
      this.settings.city = undefined;
      this.otherLocationSelected = false;
      this.settings.trainingLocation = undefined;
    }
    else if (this.settings.city !== undefined)
      this._trainingLocationAutoComplete = `${this.settings.city.label} (${this.settings.city.postCode})`;
    else if (this.otherLocationSelected)
      this._trainingLocationAutoComplete = '--- Autre ---';
    else
      this._trainingLocationAutoComplete = undefined;
    this.locationSuggestions = undefined;
  }

  selectLocation(city: CityViewModel) {
    clearTimeout(this.locationFetchTimeout);
    this.otherLocationSelected = false;
    this.settings.city = city;
    this.settings.trainingLocation = undefined;
    this.locationSuggestions = undefined;
    this._trainingLocationAutoComplete = `${city.label} (${city.postCode})`;
    setTimeout(() => this._markSettingsFormAsDirty());
  }

  selectOtherLocation() {
    clearTimeout(this.locationFetchTimeout);
    this.otherLocationSelected = true;
    this.settings.city = undefined;
    this.locationSuggestions = undefined;
    this._trainingLocationAutoComplete = '--- Autre ---';
    this._markSettingsFormAsDirty();
  }
  //#endregion

  //#region Diploma
  private _diplomaAutoComplete?: string;
  otherDiplomaSelected: boolean = false;
  get diplomaAutoComplete(): string | undefined {
    return this._diplomaAutoComplete;
  }

  private diplomaFetchTimeout: any;
  diplomaSuggestions?: DiplomaViewModel[] = undefined;
  diplomaSelectedSuggestionIndex: number = 0;

  set diplomaAutoComplete(value: string | undefined) {
    this._diplomaAutoComplete = value;

    clearTimeout(this.diplomaFetchTimeout);
    this.diplomaFetchTimeout = setTimeout(async () => {
      this.diplomaSelectedSuggestionIndex = 0;
      if (!!value)
        this.diplomaSuggestions = await lastValueFrom(this._apiService.indexDiplomas(value));
      else
        this.diplomaSuggestions = [];
    }, 200);
  }

  onDiplomaKeyPressed(event: KeyboardEvent) {
    if (event.key.toLowerCase() === 'arrowup') {
      if (--this.diplomaSelectedSuggestionIndex < 0 && this.diplomaSuggestions !== undefined)
        this.diplomaSelectedSuggestionIndex = this.diplomaSuggestions.length;
    }
    else if (event.key.toLowerCase() === 'arrowdown') {
      if (this.diplomaSuggestions !== undefined && ++this.diplomaSelectedSuggestionIndex > this.diplomaSuggestions.length)
        this.diplomaSelectedSuggestionIndex = 0;
    }
    else if (event.key.toLowerCase() === 'escape')
      this.diplomaSuggestions = undefined;
    else if (event.key.toLowerCase() === 'enter' && this.diplomaSuggestions !== undefined) {
      if (this.diplomaSelectedSuggestionIndex === this.diplomaSuggestions.length)
        this.selectOtherDiploma();
      else
        this.selectDiploma(this.diplomaSuggestions[this.diplomaSelectedSuggestionIndex]);
    }
    else
      return;

    event.preventDefault();
    event.stopPropagation();
    event.stopImmediatePropagation();
  }

  onBlurDiploma() {
    clearTimeout(this.diplomaFetchTimeout);
    if (!this._diplomaAutoComplete) {
      this._diplomaAutoComplete = undefined;
      this.settings.diploma = undefined;
      this.otherDiplomaSelected = false;
      this.settings.trainingLabel = undefined;
    }
    else if (this.settings.diploma !== undefined)
      this._diplomaAutoComplete = this.settings.diploma.label;
    else if (this.otherDiplomaSelected)
      this._diplomaAutoComplete = '--- Autre ---';
    else
      this._diplomaAutoComplete = undefined;
    this.diplomaSuggestions = undefined;
  }

  selectDiploma(diploma: DiplomaViewModel) {
    clearTimeout(this.diplomaFetchTimeout);
    this.otherDiplomaSelected = false;
    this.settings.diploma = diploma;
    this.settings.trainingLabel = undefined;
    this.diplomaSuggestions = undefined;
    this._diplomaAutoComplete = diploma.label;
    this._markSettingsFormAsDirty();
  }

  selectOtherDiploma() {
    clearTimeout(this.diplomaFetchTimeout);
    this.otherDiplomaSelected = true;
    this.settings.diploma = undefined;
    this.diplomaSuggestions = undefined;
    this._diplomaAutoComplete = '--- Autre ---';
    this._markSettingsFormAsDirty();
  }
  //#endregion

  //#endregion
}
