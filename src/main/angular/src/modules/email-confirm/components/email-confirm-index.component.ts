import {Component, OnInit} from "@angular/core";
import {ApiException, ApiService, EmailConfirmationRequestViewModel} from "../../shared/services/api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {lastValueFrom} from "rxjs";

@Component({
  templateUrl: './email-confirm-index.component.html'
})
export class EmailConfirmIndexComponent implements OnInit {

  state: 'pending' | 'success' | 'error' = 'pending';

  constructor(
    private readonly _apiService: ApiService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _router: Router
  ) {
  }

  async ngOnInit(): Promise<void> {
    const code = this._activatedRoute.snapshot.queryParams['code'];
    if (code === undefined) {
      await this._router.navigate(['/']);
      return;
    }

    try {
      await lastValueFrom(this._apiService.storeEmailConfirmation(new EmailConfirmationRequestViewModel({emailConfirmationToken: code})));
      this.state = 'success';
    }
    catch (e) {
      if (!ApiException.isApiException(e) || e.status !== 400)
        throw e;

      this.state = 'error';
    }
  }
}
