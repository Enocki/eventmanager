import {NgModule} from '@angular/core';
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";
import {EmailConfirmIndexComponent} from "./components/email-confirm-index.component";


@NgModule({
  declarations: [
    EmailConfirmIndexComponent
  ],
  imports: [
    SharedModule,

    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        component: EmailConfirmIndexComponent
      }
    ])
  ]
})
export class EmailConfirmModule { }
