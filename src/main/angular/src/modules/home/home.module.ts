import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeIndexComponent } from './components/home-index.component';
import {RouterLink} from "@angular/router";



@NgModule({
  declarations: [
    HomeIndexComponent
  ],
  exports: [
    HomeIndexComponent
  ],
  imports: [
    CommonModule,
    RouterLink
  ]
})
export class HomeModule { }
