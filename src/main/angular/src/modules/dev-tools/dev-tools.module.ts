import {NgModule} from "@angular/core";
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";
import {ExampleFormComponent} from "./components/example-form.component";
import {MockInjectorIndexComponent} from "./components/mock-injector-index.component";

@NgModule({
  declarations: [
    ExampleFormComponent,
    MockInjectorIndexComponent
  ],
  imports: [
    SharedModule,

    RouterModule.forChild([
      {
        path: 'example-form',
        pathMatch: 'full',
        component: ExampleFormComponent
      },
      {
        path: 'mock',
        pathMatch: 'full',
        component: MockInjectorIndexComponent
      }
    ])
  ],
  exports: []
})
export class DevToolsModule { }
