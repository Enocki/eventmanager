import {Component, HostBinding, HostListener, Input} from "@angular/core";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'e-checkbox',
  template: '<input type="checkbox" [name]="name ?? \'\'" [id]="inputId ?? \'\'" class="d-none" (click)="onClick($event)" />',
  styles: `
    :host {
      position: relative;

      display: block;
      height: 1.25em;
      width: 1.25em;

      border: 2px solid black;
      border-radius: .15em;

      &.checked, &:hover {
        box-shadow: 2px 2px black;
      }

      &.checked {
        &:before {
          content: '';
          position: absolute;
          top: 0;
          left: 25%;

          display: block;
          width: 50%;
          height: 75%;

          border-right: 2px solid black;
          border-bottom: 2px solid black;

          transform: rotate(45deg);
        }
      }

      &.disabled {

      }
    }
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: CheckboxComponent
    }
  ]
})
export class CheckboxComponent implements ControlValueAccessor {
  @HostBinding('class.checked')
  checked: boolean = false;

  @HostBinding('class.disabled')
  disabled: boolean = false;

  @Input('inputId') inputId?: string;
  @Input('name') name?: string;

  @HostListener('click', ['$event'])
  onClick(event: MouseEvent): void {
    event.preventDefault();
    event.stopPropagation();
    event.stopImmediatePropagation();

    if (this.disabled)
      return;

    this.checked = !this.checked;
    this._onTouched();
    this._onChange(this.checked);
  }

  writeValue(value: boolean) {
    this.checked = value;
  }

  private _onChange: (value: boolean) => void = () => {};
  registerOnChange(fn: (value: boolean) => void) {
    this._onChange = fn;
  }

  private _onTouched: () => void = () => {};
  registerOnTouched(fn: () => void) {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
