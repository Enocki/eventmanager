import {ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {inject} from "@angular/core";
import {AuthenticationService} from "../services/authentication.service";

export const anonymousGuard: CanActivateFn = (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree => {
  return new Promise(async accept => {
    const router = inject(Router);
    try {
      const user = await inject(AuthenticationService).getCurrentUserPromise();
      if (user != null)
        await router.navigate(['/']);
      accept(user == null);
    }
    catch (e) {
      await router.navigate(['/']);
      accept(true)
    }
  });
};
