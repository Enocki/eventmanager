import {Directive} from "@angular/core";

@Directive({
  selector: 'button[big-button], input[type=submit][big-button], input[type=button][big-button]',
  host: {
    '[class.big-button]': 'true'
  }
})
export class BigButtonDirective {
  constructor() {}
}
