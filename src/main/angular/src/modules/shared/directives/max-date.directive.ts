import {Directive, forwardRef, Input} from "@angular/core";
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";

@Directive({
  selector: 'input[type=date][dateValue][maxDate]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => MaxDateDirective),
    multi: true
  }]
})
export class MaxDateDirective implements Validator {

  private _maxDate?: Date;

  @Input()
  set maxDate(value: string | Date | undefined) {
    if (value === undefined) {
      this._maxDate = undefined;
      return;
    }

    if (value instanceof Date)
      this._maxDate = value;
    else
      this._maxDate = new Date(value);

    this._onValidatorChange();
  }

  validate(control: AbstractControl): ValidationErrors | null {
    if (!control.value || !this._maxDate)
      return null;

    if (!(control.value instanceof Date))
      return null;

    if (control.value > this._maxDate)
      return { maxDate: true };

    return null;
  }

  private _onValidatorChange: () => void = () => {};
  registerOnValidatorChange(fn: () => void) {
    this._onValidatorChange = fn;
  }
}
