import {Directive, forwardRef, Input} from "@angular/core";
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";

@Directive({
  selector: 'input[type=password][passwordConfirmation]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => PasswordConfirmationDirective),
    multi: true
  }]
})
export class PasswordConfirmationDirective implements Validator {

  private _password?: string;

  @Input()
  set passwordConfirmation(value: string | undefined) {
    this._password = value;
    this._onValidatorChange();
  }

  validate(control: AbstractControl): ValidationErrors | null {
    if (control.value !== this._password)
      return { passwordConfirmation: true };

    return null;
  }

  private _onValidatorChange: () => void = () => {};
  registerOnValidatorChange(fn: () => void) {
    this._onValidatorChange = fn;
  }
}
