import {Component, HostBinding} from "@angular/core";
import {Modal} from "../classes/modal";
import {Subject} from "rxjs";
import {SettingsService} from "../services/settings.service";
import {DomSanitizer, SafeUrl} from "@angular/platform-browser";
import {DocumentUrlPipe} from "../pipes/document-url.pipe";

export type FileModalConfig = {
  fileContent?: string;
  fileType?: string;
  documentId?: number;
};

@Component({
  template: `
    @if (objectIsReady) {
        <object [data]="objectData" [type]="objectType" [ngClass]="{'w-100percent': isObjectPdf, 'max-w-100percent max-h-100percent': !isObjectPdf}"></object>
    }
  `,
  host: {
    class: 'flex justify-center'
  },
  styles: `:host { min-width: 0; min-height: 0; }`
})
export class FileModal implements Modal<FileModalConfig, undefined> {
  objectIsReady: boolean = false;

  objectData: string | SafeUrl = '';
  objectType: string = '';

  @HostBinding('class.flex-fill')
  get isObjectPdf(): boolean {
    return this.objectType === 'application/pdf';
  }

  set input(value: FileModalConfig | undefined) {
    if (!value || !value.fileType) return;

    this.objectType = value.fileType;
    if (!!value.documentId) {
      console.log("called");
      this.objectData = this._sanitizer.bypassSecurityTrustResourceUrl(new DocumentUrlPipe(this._settingsService).transform(value.documentId));
      this.objectIsReady = true;
    }
    else if (!!value.fileContent) {
      fetch('data:' + value.fileType + ';base64,' + value.fileContent)
        .then(res => res.blob())
        .then(blob => {
          this.objectData = this._sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(blob));
          this.objectIsReady = true;
        });
    }
  }
  output!: Subject<undefined>;

  constructor(
    private readonly _sanitizer: DomSanitizer,
    private readonly _settingsService: SettingsService
  ) {
  }
}
