import {Component, OnInit} from "@angular/core";
import {Modal} from "../classes/modal";
import {ErrorViewModel} from "../services/api.service";
import {Subject} from "rxjs";

@Component({
  template: `
    <pre style="margin: 0; padding: 0;">{{ errorMessage }}</pre>
  `,
  host: {
    style: 'overflow: auto;'
  }
})
export class ErrorModal implements Modal<ErrorViewModel | undefined, undefined>, OnInit {
  input?: ErrorViewModel;
  output!: Subject<undefined>;

  errorMessage: string = 'Nothing to show...';

  ngOnInit() {
    this.errorMessage = JSON.stringify(this.input, null, 2)
      .replaceAll('\\n', '\n')
      .replaceAll('\\t', '')
      .replaceAll('\\r', '');
  }
}
