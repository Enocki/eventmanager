import {Pipe, PipeTransform} from "@angular/core";
import {EmailPlaceholderViewModelType} from "../services/api.service";

@Pipe({
  name: 'placeholderType'
})
export class PlaceholderTypePipe implements PipeTransform {
  transform(placeholderType: EmailPlaceholderViewModelType): string {
    switch (placeholderType) {
      case EmailPlaceholderViewModelType.TEXT:
        return 'Texte';
      case EmailPlaceholderViewModelType.LINK:
        return 'Lien';
      default:
        return '-';
    }
  }
}
