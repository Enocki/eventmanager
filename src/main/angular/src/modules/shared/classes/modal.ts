import {Subject} from "rxjs";

export interface Modal<I, O> {
  input?: I;
  output: Subject<O | undefined>;
}
