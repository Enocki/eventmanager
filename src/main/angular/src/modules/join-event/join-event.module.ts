import {NgModule} from "@angular/core";
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";
import {JoinEventShowComponent} from "./components/join-event-show.component";

@NgModule({
  declarations: [
    JoinEventShowComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: ':eventCode',
        component: JoinEventShowComponent
      }
    ])
  ]
})
export class JoinEventModule {

}
