import {Component, OnInit} from "@angular/core";
import {
  ApiService,
  ErrorViewModelDetail,
  EventCodeJoinedByUserIdRequestViewModel
} from "../../shared/services/api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {lastValueFrom} from "rxjs";
import {BadRequestUtils} from "../../shared/classes/badrequest-utils";

@Component({
  template: `
    @if (status === 'loading') {
      Chargement...
    } @else if (status === 'wrong-code') {
      Code invalide
    } @else if (status === 'already-in-event') {
      Vous avez déjà rejoin cet évènement !
    }
  `
})
export class JoinEventShowComponent implements OnInit {
  status: 'loading' | 'wrong-code' | 'already-in-event' = 'loading';

  constructor(
    private readonly _apiService: ApiService,
    private readonly _route: ActivatedRoute,
    private readonly _router: Router
  ) { }

  async ngOnInit() {
    const eventCode = this._route.snapshot.params['eventCode'];
    if (eventCode == null || !/^[a-zA-Z0-9]{8}$/.test(eventCode)) {
      this.status = 'wrong-code';
      return;
    }

    try {
      await lastValueFrom(this._apiService.addCurrentUserToEventByCode(new EventCodeJoinedByUserIdRequestViewModel({ eventCode: eventCode })));
      await this._router.navigate(["/"]);
    }
    catch (error: any) {
      const viewModel = BadRequestUtils.asBadRequestException(error);
      switch (viewModel.detail)
      {
        case ErrorViewModelDetail.UNKNOWN_EVENT:
          this.status = 'wrong-code';
          break;

        case ErrorViewModelDetail.EVENT_ALREADY_JOINED:
          this.status = 'already-in-event';
          break;
      }
    }
  }
}
