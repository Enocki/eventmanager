import {Component, OnInit} from "@angular/core";
import {
  ApiService,
  EmailPlaceholderViewModel,
  EmailTemplateViewModel,
  PlaceholderContextViewModel
} from "../../shared/services/api.service";
import {lastValueFrom} from "rxjs";
import {ModalService} from "../../shared/services/modal.service";
import {AuthenticationService} from "../../shared/services/authentication.service";

@Component({
  templateUrl: './email-template-index.component.html',
  host: {
    class: 'content-width'
  },
})
export class EmailTemplateIndexComponent implements OnInit {

  templates: EmailTemplateViewModel[] = [];
  newTemplateName?: string;

  constructor(
    private readonly _apiService: ApiService,
    private readonly _authenticationService: AuthenticationService,
    private readonly _modalService: ModalService
) { }

  async ngOnInit(): Promise<void> {
    this.templates = (await lastValueFrom(this._apiService.indexEmailTemplate(undefined))).templates;
  }

  private _currentTemplate?: EmailTemplateViewModel;
  private _currentTemplateContent?: string;
  private _currentTemplateSubject?: string;
  private _currentTemplateShared?: boolean;

  get currentTemplate(): EmailTemplateViewModel | undefined {
    return this._currentTemplate;
  }

  set currentTemplate(value: EmailTemplateViewModel | undefined) {
    this._currentTemplate = value;
    this._currentTemplateContent = value?.content;
    this._currentTemplateSubject = value?.subject;
    this._currentTemplateShared = value?.shared;
  }

  get currentTemplatePlaceholders(): EmailPlaceholderViewModel[] { return this._currentTemplate?.placeholders ?? []; }

  get currentTemplateContent(): string | undefined { return this._currentTemplateContent; }
  set currentTemplateContent(value: string | undefined) { this._currentTemplateContent = value; }

  get currentTemplateSubject(): string | undefined { return this._currentTemplateSubject; }
  set currentTemplateSubject(value: string | undefined) { this._currentTemplateSubject = value; }

  get currentTemplateShared(): boolean | undefined { return this._currentTemplateShared; }
  set currentTemplateShared(value: boolean | undefined) { this._currentTemplateShared = value; }

  get currentTemplateDirty(): boolean {
    return this._currentTemplateContent !== this._currentTemplate?.content || this._currentTemplateSubject !== this._currentTemplate?.subject || this._currentTemplateShared !== this._currentTemplate?.shared;
  }

  get currentTemplateEditable(): boolean {
    return !this._currentTemplate?.ownerId || this._currentTemplate.ownerId === this._authenticationService.currentUser?.id;
  }

  get currentTemplateDeletable(): boolean {
    return this.currentTemplateEditable && !this._currentTemplate?.code;
  }

  get currentTemplateAdmin(): boolean {
    return !!this._currentTemplate?.code;
  }

  async createNewTemplate(): Promise<void> {
    if (!this.newTemplateName)
      return;

    const newTemplate = await lastValueFrom(this._apiService.setEmailTemplate(new EmailTemplateViewModel({
      subject: '[EventManager] Mail',
      content: 'Mon super modèle d\'email',
      label: this.newTemplateName,
      shared: false,
      placeholders: []
    })));

    this.templates.push(newTemplate);
    this.currentTemplate = newTemplate;

    this.newTemplateName = undefined;
  }

  async save(): Promise<void> {
    if (!this._currentTemplate || !this._currentTemplateContent || !this._currentTemplateSubject)
      throw new Error("No template to save");

    this._currentTemplate.content = this._currentTemplateContent;
    this._currentTemplate.subject = this._currentTemplateSubject;
    this._currentTemplate.shared = this._currentTemplateShared ?? false;
    await lastValueFrom(this._apiService.setEmailTemplate(this._currentTemplate));
  }

  cancel(): void {
    this._modalService.openYesNoModal(
      'Êtes-vous sûr ?',
      'Toutes les modifications que vous avez apportées au modèle seront perdu.',
      validated => {
        if (validated) {
          this._currentTemplateContent = this.currentTemplate?.content;
          this._currentTemplateSubject = this.currentTemplate?.subject;
          this._currentTemplateShared = this.currentTemplate?.shared;
        }
      }
    );
  }

  delete(): void {
    this._modalService.openYesNoModal(
      'Êtes-vous sûr ?',
      'Le modèle sera définitivement perdu!',
      async validated => {
        if (validated && !!this._currentTemplate?.id) {
          await lastValueFrom(this._apiService.deleteEmailTemplate(this._currentTemplate.id));
          this.templates = this.templates.filter(template => template.id !== this._currentTemplate?.id);
          this.currentTemplate = undefined;
        }
      }
    );
  }

  async sendSampleEmail(): Promise<void> {
    if (!this._currentTemplateContent || !this._currentTemplateSubject)
      throw new Error('Content or subject undefined');

    const filledTemplate = new EmailTemplateViewModel(this._currentTemplate);
    filledTemplate.content = this._currentTemplateContent;
    filledTemplate.subject = this._currentTemplateSubject;

    await lastValueFrom(this._apiService.storeSendEmailTemplateRequest(filledTemplate));
  }
}
