@echo off

echo.
echo Cleaning...
rmdir /s /q output
rmdir /s /q src\main\angular\dist
mkdir output

echo.
echo Building Jar...
call .\gradlew bootJar

echo.
echo.
echo Building Angular App...
cd src/main/angular
call ng build --configuration=production
cd ../../..

echo.
echo.
echo Copying files...
mkdir output\clientapp
Xcopy /E /I  src\main\angular\dist output\clientapp
move output\clientapp\assets\settings.prod.json output\clientapp\assets\settings.json

mkdir output\serverapp
echo F | Xcopy build\libs\*.jar output\serverapp\EventManager.jar
copy src\main\resources\application.prod.properties output\serverapp\application.properties
Xcopy /E /I  src\main\resources\migrations output\serverapp\migrations

(echo FROM openjdk:21 & echo ENV JAR_FILE="" & echo ENV JAVA_OPTS="" & echo WORKDIR /usr/src/myapp & echo CMD ["sh", "-c", "java -jar $JAR_FILE $JAVA_OPTS"]) > output\Dockerfile
(echo RewriteEngine On & echo RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -f [OR] & echo RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -d & echo RewriteRule ^^ - [L] & echo RewriteRule ^^ /index.html) > output\clientapp\.htaccess
(echo version: '3.4' & echo name: eventmanager & echo services: & echo   eventmanager-backend: & echo     build: ./ & echo     container_name: eventmanager-backend & echo     restart: always & echo     ports: & echo       - "8080:8080" & echo     environment: & echo       - JAR_FILE=EventManager.jar & echo     volumes: & echo       - ./serverapp:/usr/src/myapp & echo     stdin_open: true & echo     tty: true) > output\docker-compose.yml

echo.
echo.
echo Thanks, Good Bye!
